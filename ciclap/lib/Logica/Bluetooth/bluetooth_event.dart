import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BluetoothEvent extends Equatable{

  BluetoothEvent([List props = const[]]): super(props);
}



class Refresh extends BluetoothEvent{

}