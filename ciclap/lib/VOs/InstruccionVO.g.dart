// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'InstruccionVO.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class InstruccionAdapter extends TypeAdapter<Instruccion> {
  @override
  Instruccion read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Instruccion(
      instruction: fields[3] as String,
      duration: fields[1] as double,
      distance: fields[0] as double,
      name: fields[4] as String,
      type: fields[2] as double,
    );
  }

  @override
  void write(BinaryWriter writer, Instruccion obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.distance)
      ..writeByte(1)
      ..write(obj.duration)
      ..writeByte(2)
      ..write(obj.type)
      ..writeByte(3)
      ..write(obj.instruction)
      ..writeByte(4)
      ..write(obj.name);
  }
}
