import 'dart:async';
import 'dart:math';
import 'package:bloc/bloc.dart';
import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/Repositorio/AutenticacionRepo.dart';
import 'package:ciclap/VOs/UsuarioVO.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';


class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final AutenticacionRepo _userRepository;

  RegisterBloc({@required AutenticacionRepo userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  RegisterState get initialState => RegisterState.empty();

  @override
  Stream<RegisterState> transformEvents(
      Stream<RegisterEvent> events,
      Stream<RegisterState> Function(RegisterEvent event) next,
      ) {
    final observableStream = events as Observable<RegisterEvent>;
    final nonDebounceStream = observableStream.where((event) {
      return (event is! EmailChangedRegistro && event is! PasswordChangedRegistro);
    });
    final debounceStream = observableStream.where((event) {
      return (event is EmailChangedRegistro || event is PasswordChangedRegistro);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(nonDebounceStream.mergeWith([debounceStream]), next);
  }

  @override
  Stream<RegisterState> mapEventToState(
      RegisterEvent event,
      ) async* {
    if (event is SubmittedRegistro) {
      yield* _mapFormSubmittedToState(event.email, event.password, event.nombre, event.peso, event.fecha);
    }
  }



  Stream<RegisterState> _mapFormSubmittedToState(
      String email,String password,String nombre, int peso, String fecha) async* {
    yield RegisterState.loading();
    try {

      Usuario usuario = Usuario(nombre, "Principiante", 0, 0, 0, fecha, peso, 0);
      print(nombre);
      print(email);
      await _userRepository.signUp(
        email: email.trim(),
        password: password,
        usuario: usuario
      );
      yield RegisterState.success();
    } catch (error) {
      print(error);
      String errorP = "Error en registro";
      if(error.toString().contains("badly formatted"))
        {
          errorP = "El mail tiene un mal formato";
        }else if(error.toString().contains("")){

        errorP = "Ya hay una cuenta con ese mail";
      }
      yield RegisterState.failure(errorP);
    }
  }
}