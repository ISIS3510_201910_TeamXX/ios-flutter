import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:ciclap/Repositorio/RutaRepo.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import '../bloc.dart';

class RutasBloc extends Bloc<RutasEvent, RutasState> {
  @override
  RutasState get initialState => InitialRutasState();
  RutaRepo rutaRepo = RutaRepo();
  @override
  Stream<RutasState> mapEventToState(
    RutasEvent event,
  ) async* {

    if(event is GetRutas)
      {
        print("Entra");

          List<Ruta> rutas = await rutaRepo.getRutasUser(event.uid);
          print("jijiji");
          yield RutasLoaded(rutas);


      }

  }
}
