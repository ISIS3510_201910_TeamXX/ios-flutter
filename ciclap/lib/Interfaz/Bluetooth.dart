import 'package:ciclap/Interfaz/widgets.dart';
import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/Logica/bloc.dart' as b;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blue/flutter_blue.dart';


class Bluetooth extends StatefulWidget {
  @override
  _BluetoothState createState() => _BluetoothState();
}

class _BluetoothState extends State<Bluetooth> {
  bool visible = false;
  BluetoothBloc bloc = BluetoothBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
          [Icon(Icons.directions_bike), Icon(Icons.directions_bike)],
          "Bluetooth"),
      body: BlocProvider(
        builder: (context) => bloc,
        child: body(),

      ),
    );
  }


}

class body extends StatefulWidget {
  @override
  _bodyState createState() => _bodyState();
}

class _bodyState extends State<body> {

  List<String> devices = ["Iphone", "Hola", "huawei"];

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: BlocProvider.of<BluetoothBloc>(context),
      listener: (context, BluetoothEstado estado) {
        if(estado is PrendaBlueTooth)
          {
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Por favor prenda el bluetooth")));
          }

      },
      child: Column(
        children: <Widget>[

          SizedBox(height: MediaQuery.of(context).size.height * 0.10,),
          Row(
              children: <Widget>[
                SizedBox(width: 30,),
                Icon(Icons.bluetooth_searching),
                SizedBox(width: 25,),
                Text("Buscar ", style: TextStyle(fontSize: 20),),
                Expanded(child: Container()),
                BlocBuilder(

                  bloc: BlocProvider.of<BluetoothBloc>(context),
                  builder: (context, BluetoothEstado estado) {

                   print("Entra al build");


                    if(estado is Cargando)
                      {
                        print("Entra al cargando");
                        return Container();
                      }else{
                        return FlatButton(child: Icon(
                        CupertinoIcons.refresh, size: 35, color: Colors.white,),
                        onPressed: () {

                        BlocProvider.of<BluetoothBloc>(context).dispatch(Refresh());
                        },
                        color: Colors.blueAccent,);
                        }
                    }

                ),
                SizedBox(width: 50,)
              ]
          ),

          SizedBox(height: MediaQuery.of(context).size.height * 0.08,),

          BlocBuilder(
              bloc: BlocProvider.of<BluetoothBloc>(context) ,
              builder: (context, b.BluetoothEstado estado) {
                if (estado is InitialBluetoothState) {
                  return Container();
                }
                return Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("Buscando Dispositivos...",
                        style: TextStyle(fontSize: 16, color: Colors.grey),),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.4,
                      child: ListView.builder(itemCount: devices.length,
                          itemBuilder: (context, int index) {
                            return (ListTile(
                              onTap: () {},
                              leading: Icon(Icons.devices),
                              title: Text("ss"),
                              trailing: Icon(Icons.link, color: Colors.grey,),
                            ));
                          }),
                    )
                  ],
                );
              }
          ),
        ],
      ),
    );
  }
}
