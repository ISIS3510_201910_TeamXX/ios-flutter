import 'package:ciclap/Interfaz/widgets.dart';
import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Historial extends StatefulWidget {
  Historial({Key key}) : super(key: key);

  _HistorialState createState() => _HistorialState();
}

class _HistorialState extends State<Historial> {

  RutasBloc rutasBloc = RutasBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(
          [
            Icon(
              Icons.history,
              size: 50,
            )
          ],
          "Historial",
        ),
        body: BlocBuilder(
          bloc: rutasBloc..dispatch(GetRutas((BlocProvider.of<AutenticacionBloc>(context).currentState as Authenticated).uid)),
          builder:(BuildContext context, RutasState estado)
            {
              if(estado is InitialRutasState)
                {
                  return Center(child: CupertinoActivityIndicator(radius: 20,));
                }
              else if(estado is RutasLoaded) {
                List<Ruta> rutas = estado.rutas;

                if(rutas.isEmpty)
                  {
                    return Center(child: Text("Parece que no hay nada aquí"));
                  }

                return ListView.builder(
                    itemCount: rutas.length,
                    itemBuilder: (BuildContext context, int index) {
                      return HistoryCard(
                          date: rutas[index].timeStamp.toDate()
                              .toString()
                              .split(".")[0],
                          from: rutas[index].puntoSalida.nombre,
                          to: rutas[index].puntoFinal.nombre);
                    });
              }else{

                return Center(child: Column(
                  children: <Widget>[
                    Text("No se pudo obtener el historial"),
                    CupertinoButton(child: Text("Volver a Cargar"),onPressed: () => rutasBloc.dispatch(GetRutas((BlocProvider.of<AutenticacionBloc>(context).currentState as Authenticated).uid))),

                  ],

                ) ,);

              }

            },
        ));
  }
}
