import 'package:ciclap/Interfaz/Mapa.dart';
import 'package:ciclap/Interfaz/MapaOffline.dart';
import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/VOs/LugarVO.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ciclap/Interfaz/widgets.dart';
import 'package:ciclap/Interfaz/Navegacion.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ciclap/Interfaz/Preferencias.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:connectivity/connectivity.dart';
import 'package:hive/hive.dart';

const Color negro = Color(0xFF353531);
const Color amarillo = Color(0xFFFBBC05);
const Color morado = Color(0xFF5F0F40);
const Color amarillo2 = Color(0xFFDEB642);

class VisualizarTerreno extends StatefulWidget {
  final Ruta ruta;
  final bool conParada;

  VisualizarTerreno({Key key, @required this.ruta, @required this.conParada})
      : super(key: key);

  @override
  _VisualizarTerrenoState createState() => _VisualizarTerrenoState();
}

class _VisualizarTerrenoState extends State<VisualizarTerreno> {
  @override
  final terrenoBloc = VerTerrenoBloc();
  GoogleMapa mapa = GoogleMapa(true);



  Widget build(BuildContext context) {

    if(widget.ruta.coordenadas!=null){
      persistir();
      }
    {




      terrenoBloc.dispatch(CargarRuta(widget.ruta));
      return Scaffold(
        backgroundColor: Colors.white,
        body: BlocListener(
            bloc: terrenoBloc,
            listener: (BuildContext contexto, VerTerrenoState estado){
              if(estado is Navegando){
                print("Entra");
                Navigator.push(context,MaterialPageRoute(builder: (context) =>Navegacion(ruta: estado.ruta, destino: "", terrenoBloc: terrenoBloc,)));
              }

            },
            child: BlocBuilder(
                bloc: terrenoBloc,
                builder:(BuildContext context, VerTerrenoState estado)
                {

                  return Builder(
                      builder: (context) => Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              SafeArea(
                                  child: Align(
                                    alignment: Alignment.center,
                                    heightFactor: 1.5,
                                    child: Image.asset(
                                      "images/logoAmarillo.png",
                                      height: 30,
                                    ),
                                  )),
                              SafeArea(
                                child: Row(
                                  children: <Widget>[
                                    BackButton(),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          BlocBuilder(
                            bloc: terrenoBloc,
                            builder: (BuildContext contexto, VerTerrenoState estado) {
                              return Column(
                                children: <Widget>[
                                  SearchedBar(
                                    widget.ruta.puntoSalida.nombre,
                                  ),
                                  Visibility(
                                    child: SearchedBar( builderParada()),
                                    visible: widget.conParada,
                                  ),
                                  SearchedBar(
                                    widget.ruta.puntoFinal.nombre,
                                  ),
                                ],
                              );
                            },
                          ),

                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height * 0.35,
                            decoration: BoxDecoration(
                                border: Border.all(color: Color(0xfff0f0f0))),
                            child: BlocProvider<VerTerrenoBloc>(
                                builder: (context) => terrenoBloc, child:  mapa),
                          ),

                          Container(
                              width: MediaQuery.of(context).size.width * 0.85,
                              height: MediaQuery.of(context).size.height * 0.28,
                              child: ListView(
                                padding:
                                EdgeInsets.symmetric(vertical: 5, horizontal: 0),
                                children: <Widget>[
                                  /*Grafico(
                                    color1: Color(0xFFE0FFBC),
                                    color2: Color(0xFF6878AF),
                                    color3: Color(0xFF00EEFF),
                                    textoPrincipal: "Distancia Recorrida (Proximamente)",
                                    texto1: "Cicloruta",
                                    texto2: "Calles",
                                    texto3: "Andenes",
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Grafico(
                                    color1: Color(0xFFFCFF94).withOpacity(0.75),
                                    color2: Color(0xFFFFC2C2),
                                    color3: Color(0xFFB3FCAA),
                                    textoPrincipal: "Contaminación (Proximamente)",
                                    texto1: "Alta",
                                    texto2: "Media",
                                    texto3: "Baja",
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),*/
                                  Atributo(
                                    texto: "Tiempo",
                                    valor: widget.ruta.duracion,
                                    icono: Icon(
                                      Icons.timer,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Atributo(
                                    texto: "Calorías",
                                    valor: widget.ruta.calorias,
                                    icono: Icon(
                                      Icons.whatshot,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Atributo(
                                    texto: "Distancia",
                                    valor: widget.ruta.distancia,
                                    asset: "images/regla.png",
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  FadeInImage(placeholder: AssetImage("images/placeholder.png"), image: NetworkImage("http://open.mapquestapi.com/elevation/v1/chart?key=i8K1i83X5BohGVxI61HAJx1aRMpXn9VI&shapeFormat=raw&width=325&height=250&latLngCollection=${widget.ruta.getFormatoAltura()}"))


                                ],
                              )),
                          Expanded(child: Container()),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              BotonPrimario(
                                texto: "Comenzar Viaje",
                                color: amarillo,
                                onPressed: () {
                                  terrenoBloc.dispatch(Navegar(widget.ruta, mapa));
                                },
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              /*  BotonSecundario(
                                onPressed: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Preferencias())),
                                texto: "Preferencias",
                              )*/
                            ],
                          ),
                          SizedBox(height: 20,),
                        ],
                      )
                  );}
            )
        ),
      );
    }
  }

  String builderParada() {
    if (widget.conParada) {
      return widget.ruta.puntoParada.nombre;
    }
    return "";
  }

  void persistir()async{

    final box = Hive.box("rutas");

    String key = widget.ruta.puntoSalida.ubicacion.toString()+widget.ruta.puntoFinal.ubicacion.toString();

    if(box.get(key)==null  && widget.ruta.puntoSalida.ubicacion != null) {
      widget.ruta.puntoSalida.longitud =
          widget.ruta.puntoSalida.ubicacion.longitude;
      widget.ruta.puntoSalida.latitud =
          widget.ruta.puntoSalida.ubicacion.latitude;

      widget.ruta.puntoFinal.longitud =
          widget.ruta.puntoFinal.ubicacion.longitude;
      widget.ruta.puntoFinal.latitud =
          widget.ruta.puntoFinal.ubicacion.latitude;

      box.put(key, widget.ruta);
    }
  }

}

class Grafico extends StatefulWidget {
  Color color1;
  Color color2;
  Color color3;

  String textoPrincipal = "";
  String texto1 = "";
  String texto2 = "";
  String texto3 = "";

  Grafico({
    this.color1,
    this.color2,
    this.color3,
    this.texto1,
    this.texto2,
    this.texto3,
    this.textoPrincipal,
  });

  @override
  _GraficoState createState() => _GraficoState();
}

class _GraficoState extends State<Grafico> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.85,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.textoPrincipal,
                    style: TextStyle(color: Colors.grey, fontSize: 14),
                  )
                ]),
          ),
          Row(
            children: <Widget>[
              Container(
                height: 3,
                width: MediaQuery.of(context).size.width * 0.85 * 0.3,
                decoration: BoxDecoration(color: widget.color1),
              ),
              Container(
                height: 3,
                width: MediaQuery.of(context).size.width * 0.85 * 0.4,
                decoration: BoxDecoration(color: widget.color2),
              ),
              Container(
                height: 3,
                width: MediaQuery.of(context).size.width * 0.85 * 0.3,
                decoration: BoxDecoration(color: widget.color3),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    width: 15,
                    height: 15,
                    color: widget.color1,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      widget.texto1,
                      style: TextStyle(fontSize: 10),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    width: 15,
                    height: 15,
                    color: widget.color2,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      widget.texto2,
                      style: TextStyle(fontSize: 10),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    width: 15,
                    height: 15,
                    color: widget.color3,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      widget.texto3,
                      style: TextStyle(fontSize: 10),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class Atributo extends StatefulWidget {
  String texto = "";
  String valor = "";
  String asset = "";
  Icon icono;

  Atributo({this.texto, this.valor, this.icono, this.asset});

  @override
  _AtributoState createState() => _AtributoState();
}

class _AtributoState extends State<Atributo> {
  Widget _buildChildIcono() {
    if (widget.asset == null) {
      return widget.icono;
    }
    return Image.asset(
      widget.asset,
      width: 25,
      height: 25,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.85,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  widget.texto,
                  style: TextStyle(fontSize: 16),
                ),
                Row(
                  children: <Widget>[
                    Text(widget.valor),
                    SizedBox(
                      width: 15,
                    ),
                    _buildChildIcono()
                  ],
                )
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.85,
              height: 1,
              color: amarillo2,
            )
          ],
        ));
  }
}
