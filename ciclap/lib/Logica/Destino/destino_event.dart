import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/VOs/LugarVO.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DestinoEvent extends Equatable{

  DestinoEvent([List props = const[]]): super(props);

}

class Cargar extends DestinoEvent{
  bool conParada;
  Cargar(this.conParada):super([conParada]);
}


class AgregarParada extends DestinoEvent
{

    AgregarParada():super();
}

class SeleccionarReciente extends DestinoEvent
{
  final Lugar lugar;

    SeleccionarReciente(this.lugar):super([lugar]);
}

class SeleccionarMiUbicacion extends DestinoEvent{

  Lugar miUbicacion;
  Lugar recienteSeleccionado;

  SeleccionarMiUbicacion({this.miUbicacion, this.recienteSeleccionado}):super([miUbicacion,recienteSeleccionado]);
}


class EnviarDestino extends DestinoEvent
{
  final Lugar lugarOrigen;
  final Lugar lugarDestino;
  final bool cicloruta;


  EnviarDestino(this.lugarOrigen,this.lugarDestino, this.cicloruta): super([lugarOrigen,lugarDestino]);

}


class EnviarDestinoConParada extends DestinoEvent
{
  final Lugar lugarOrigen;
  final Lugar lugarDestino;
  final Lugar lugarParada;
  final bool cicloruta;

  EnviarDestinoConParada({this.lugarOrigen,this.lugarDestino,this.lugarParada, this.cicloruta}):assert(lugarParada!=null && lugarDestino!=null && lugarOrigen !=null),super([lugarOrigen,lugarDestino,lugarParada]);

}

class RebuildRecientesCargados extends DestinoEvent
{
  RecientesCargados anteriorEstado;
  RebuildRecientesCargados(this.anteriorEstado):super([anteriorEstado]);
}

class RebuildSeleccionada extends DestinoEvent{
  DestinoState anteriorEstado;
  RebuildSeleccionada(this.anteriorEstado):super([anteriorEstado]);

}

class SeleccionarDeSuggestion extends DestinoEvent{
  String tipoLugar;
  Lugar lugar;

  SeleccionarDeSuggestion(this.tipoLugar, this.lugar):super([tipoLugar,lugar]);
}