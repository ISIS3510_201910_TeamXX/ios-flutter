import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RutasEvent extends Equatable {
  RutasEvent([List props = const []]) : super(props);
}

// The only event in this app is for getting the weather
class GetRutas extends RutasEvent {

  final String uid;
  // Equatable allows for a simple value equality in Dart.
  // All you need to do is to pass the class fields to the super constructor.
  GetRutas(this.uid) : super([uid]);
}


