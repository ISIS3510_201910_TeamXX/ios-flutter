import 'package:ciclap/VOs/PuntoInteresVO.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';



List<PuntoInteres> mapear(List<DocumentSnapshot> lista)
{

  return lista.map((doc) => PuntoInteres.fromMap(doc.data, doc.documentID))
    .toList();
}


class PIrepo{

  final Firestore _db = Firestore.instance;
  final String path;
  CollectionReference ref;

  PIrepo(this.path) {
    ref = _db.collection(path);
  }

  Future<List<PuntoInteres>> getPuntosInteres() async
  {

    QuerySnapshot snap = await getDataCollection();
    try {
      return compute(mapear,snap.documents);

    }catch(error){
      print(error);
    }
  }

  Future<List<PuntoInteres>> getPorTipo({tipo:String}) async
  {
    Query q = ref.where("categoria",isEqualTo: tipo);
    QuerySnapshot snap = await q.getDocuments();
    try {
      return compute(mapear,snap.documents);

    }catch(error){
      print(error);
    }
  }


  Future<void> postPI(PuntoInteres pi)
  {
    pi.numResenias = 0;

    try{

      ref.add(pi.toJson());
    }catch(error)
    {
      print("ERROR EN REPO" + error.toString());
    }


  }


  Future<QuerySnapshot> getDataCollection() {
    return ref.getDocuments();
  }
}