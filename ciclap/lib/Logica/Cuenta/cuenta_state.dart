import 'package:ciclap/VOs/UsuarioVO.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CuentaState extends Equatable{
  CuentaState([List props = const[]]): super(props);

}

class InitialCuentaState extends CuentaState {

}


class CuentaCargando extends CuentaState{

}


class CuentaCargada extends CuentaState{
  final Usuario usuario;

  CuentaCargada(this.usuario):super([usuario]);

}
