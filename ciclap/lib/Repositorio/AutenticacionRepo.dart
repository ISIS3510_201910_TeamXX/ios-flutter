import 'package:ciclap/Repositorio/UsuarioRepo.dart';
import 'package:ciclap/VOs/UsuarioVO.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AutenticacionRepo {
  final FirebaseAuth _firebaseAuth;
  final GoogleSignIn _googleSignIn;

  AutenticacionRepo({FirebaseAuth firebaseAuth, GoogleSignIn googleSignin}): _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance, _googleSignIn = googleSignin ?? GoogleSignIn();


  Future<FirebaseUser> signInConGoogle() async
  {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    await _firebaseAuth.signInWithCredential(credential);
    return _firebaseAuth.currentUser();
  }


  Future<void> signInWithCredentials(String email, String password) {
    return _firebaseAuth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  Future<void> signUp({String email, String password, Usuario usuario}) async {
    var result = await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );

    UsuarioRepo("usuarios").postUsuario(result.user.uid, usuario);
  }

  Future<void> signOut()
  {
    return Future.wait([
      _firebaseAuth.signOut(),
      _googleSignIn.signOut(),
    ]);
  }


  Future<bool> isSignedIn() async {
    final currentUser = await _firebaseAuth.currentUser();
    return currentUser != null;
  }

  Future<String> getUid() async {
    return (await _firebaseAuth.currentUser()).uid;
  }

}