import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:ciclap/VOs/UsuarioVO.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ciclap/Interfaz/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

const Color negro = Color(0xFF353531);

class Cuenta extends StatefulWidget {
  @override
  _CuentaState createState() => _CuentaState();
}

class _CuentaState extends State<Cuenta> {

  CuentaBloc cuentaBloc = CuentaBloc();
  Usuario usuario;

  @override
  initState()
  {
    super.initState();
    AutenticacionState state = BlocProvider.of<AutenticacionBloc>(context).currentState;
    Authenticated estado =  state;
    usuario = estado.usuario;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          body:Column(
              children: <Widget>[
               Stack(alignment: Alignment.center, children: <Widget>[
                Container(
                padding: EdgeInsets.zero,
                width: MediaQuery.of(context).size.width * 1,
                height: MediaQuery.of(context).size.width * 0.7,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Color(0xffF1AF47), Color(0xffEBA86F)])),
              ),
              CircleAvatar(
                radius: 80,
                backgroundColor: Colors.white,
              ),
              Positioned(
                  right: MediaQuery.of(context).size.width * 0.32,
                  top:MediaQuery.of(context).size.width * 0.5,

                  child: Container(
                    decoration: BoxDecoration(color: negro , shape: BoxShape.circle,),
                    height: 40,
                    width: 40,
                    child:Icon(Icons.camera_alt, color: Colors.white, size: 30,),

                  )),
              Positioned(
                left: 0,
                top: 20,
                child: BackButton(color: Colors.black,),
              )
        ]),

                SizedBox(height: 10,),
                Text(usuario.nombre, style: TextStyle(fontSize: 24),),
                SizedBox(height: 10 ,),
                Text(usuario.titulo, style: TextStyle(fontSize: 15),),


                Divider(height: 15, endIndent: 30, indent: 30,),

                BlocBuilder(
                  bloc: cuentaBloc,
                  builder: (BuildContext context, CuentaState estado)
                  {
                  if(estado is InitialCuentaState)
                    {
                      cuentaBloc.dispatch(CargarCuenta(usuario.id));
                    }
                  if(estado is CuentaCargando)
                    {
                      return CupertinoActivityIndicator(radius: 15,);
                    }

                  if(estado is CuentaCargada)
                      {
                        String distancia = Ruta.distanciaToString(estado.usuario.distancia) ?? "";
                        String  calorias = Ruta.caloriasToString(estado.usuario.calorias) ?? "";

                        return Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly  ,
                              children: <Widget>[
                                CuadroPerfil(asset: "images/biciMorada.png", categoria: "Distancias", textoGrande: distancia ,evitado: "",textoSecundario: "Kilómetros recorridos",),
                                CuadroPerfil(asset: "images/fuegoMorado.png", categoria: "Calorías", textoGrande: calorias,evitado: "",textoSecundario: "Calorías Quemadas",)
                              ],
                            ),
                            SizedBox(height: MediaQuery.of(context).size.width * 0.03 ,),
                            /*Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                CuadroPerfil(asset: "images/industriaMorada.png", categoria: "Polución", textoGrande: "300",evitado: "Evitado", textoSecundario: "Rutas Contaminadas",),
                                CuadroPerfil(asset: "images/editMorado.png", categoria: "Editar", textoGrande: "",evitado: "",textoSecundario: "",)
                              ],

                            )*/

                          ],
                        );
                      }
                  return Container();
                  },
              )


      ],
    )

    );
      }

  }

