import 'package:ciclap/Interfaz/Mapa.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';

@immutable
abstract class NavegarEvent extends Equatable{
  NavegarEvent([List props = const[]]): super(props);
}


class ComenzarNavegacion extends NavegarEvent{
  String uid;
  final Ruta ruta;
  ComenzarNavegacion(this.ruta,this.uid):super([ruta,uid]);

}

class SiguienteInstruccion extends NavegarEvent{
  SiguienteInstruccion():super();
}


class AgregarSticker extends NavegarEvent
{
  
}


class SeleccionarSticker extends NavegarEvent{

  final String stickerSel;

  SeleccionarSticker(this.stickerSel):super([stickerSel]);

}

class ConfirmarCancelarUbicacion extends NavegarEvent{

  final bool confirmar;
  final LatLng ubicacion;
  final GoogleMapa mapa;

  ConfirmarCancelarUbicacion(this.confirmar, this.ubicacion,this.mapa):super([confirmar,ubicacion,mapa]);

}

