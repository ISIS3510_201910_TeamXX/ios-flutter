
class Usuario
{
  String id;
  String nombre;
  String titulo;
  double distancia;
  double calorias;
  double duracion;
  double polucion;
  int peso;
  String fechaNacimiento;

  Usuario(this.nombre, this.titulo, this.distancia, this.calorias,
      this.polucion,this.fechaNacimiento, this.peso, this.duracion);


  Usuario.fromMap(Map snapshot,String id) :
        id = id ?? '',
        nombre = snapshot['nombre'] ?? "",
        titulo = snapshot['titulo'] ?? "",
        distancia = snapshot['distancia'].toDouble() ?? 0,
        calorias = snapshot['calorias'].toDouble() ?? 0,
        duracion = snapshot['duracion'].toDouble()?? 0,
        polucion = snapshot['polucion'].toDouble()?? 0,
        peso = snapshot['peso'],
        fechaNacimiento = snapshot['fechaNacimiento']?? '';


  toJson()
  {
    return{
      "id": id,
      "nombre": nombre,
      "titulo": titulo,
      "distancia": distancia,
      "duracion": duracion,
      "calorias": calorias,
      "duracion": duracion,
      "polucion": polucion,
      "peso": peso,
      "fechaNacimiento": fechaNacimiento,

    };


  }
}