import 'package:flutter/material.dart';
import 'package:ciclap/Interfaz/VisualizarTerreno.dart' as vt;
import 'package:ciclap/Interfaz/widgets.dart';
import 'package:ciclap/Interfaz/Mapa.dart' as mapa;

class AgregarRuta extends StatefulWidget {
  @override
  _AgregarRutaState createState() => _AgregarRutaState();
}

class _AgregarRutaState extends State<AgregarRuta> {
  bool cuestas = false;
  bool ciclorruta = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Builder(
        builder: (context) => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                SafeArea(
                    child: Align(
                  alignment: Alignment.center,
                  heightFactor: 1.5,
                  child: Image.asset(
                    "images/logoAmarillo.png",
                    height: 30,
                  ),
                )),
                SafeArea(
                  child: Row(
                    children: <Widget>[
                      BackButton(),
                    ],
                  ),
                ),
              ],
            ),
            SearchedBar(
               "Cra 123 #45-2",
            ),
            SearchedBar(
              "Cra 132 #32-2",
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.20,
              decoration:
                  BoxDecoration(border: Border.all(color: Color(0xfff0f0f0))),
              child: mapa.GoogleMapa(true),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Prefiero ir en ciclorruta',
                        style: TextStyle(fontSize: 20),
                      ),
                      Switch(
                        activeColor: Colors.white,
                        activeTrackColor: amarillo,
                        value: ciclorruta,
                        onChanged: (value) {
                          setState(() {
                            ciclorruta = value;
                          });
                        },
                      )
                    ],
                  ),
                  Text(
                    'Buscaremos una ruta para uque vayas la mayoría del tiempo en ciclorruta',
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Prefiero evitar cuestas',
                        style: TextStyle(fontSize: 20),
                      ),
                      Switch(
                        activeColor: Colors.white,
                        activeTrackColor: amarillo,
                        value: cuestas,
                        onChanged: (value) {
                          setState(() {
                            cuestas = value;
                          });
                        },
                      )
                    ],
                  ),
                  Text(
                    'Basado en lo que digann otros biciusuarios te mandaremos por una ruta sin cuestas',
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
                width: MediaQuery.of(context).size.width * 0.85,
                height: MediaQuery.of(context).size.height * 0.3,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    vt.Atributo(
                      texto: "Tiempo",
                      valor: "1 hr 15 min",
                      icono: Icon(
                        Icons.timer,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    vt.Atributo(
                      texto: "Calorías",
                      valor: "500 cal",
                      icono: Icon(
                        Icons.whatshot,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    vt.Atributo(
                      texto: "Dificultad",
                      valor: "Alta",
                      asset: "images/dificultad.png",
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    vt.Atributo(
                      texto: "Distancia",
                      valor: "2.4km",
                      asset: "images/regla.png",
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    vt.Atributo(
                      texto: "Peligro",
                      valor: "Bajo",
                      asset: "images/warning.png",
                    )
                  ],
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                BotonPrimario(
                    texto: "Guardar Ruta",
                    color: amarillo,
                    onPressed: () => {}),
              ],
            )
          ],
        ),
      ),
    );
  }
}
