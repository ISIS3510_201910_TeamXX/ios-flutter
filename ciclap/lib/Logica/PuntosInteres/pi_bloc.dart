import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:ciclap/Interfaz/PuntosInteres.dart';
import 'package:ciclap/Repositorio/PIrepo.dart';
import 'package:ciclap/Repositorio/RutaRepo.dart';
import 'package:ciclap/VOs/LugarVO.dart';
import 'package:ciclap/VOs/PuntoInteresVO.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import '../bloc.dart';


List<PuntosInteres> getPuntosInteres()
{

}

class PiBloc extends Bloc<PiEvent, PiState> {

  PIrepo repo = PIrepo("puntosInteres");
  RutaRepo repoRutas = RutaRepo();
  Location location = Location();
  @override
  PiState get initialState => InitialPiState();

  @override
  Stream<PiState> mapEventToState(
    PiEvent event,
  ) async* {
    if(event is CargarPi)
      {
          List<PuntoInteres> puntos =await  repo.getPorTipo(tipo:"Taller");
          List<PuntoInteres> todos = await repo.getPuntosInteres();
          yield CargadosPI(puntos, null,true,false,false);

      }

    if(event is CargarCafe)
      {
        CargadosPI actual = currentState;
        List<PuntoInteres> cafeterias =await  repo.getPorTipo(tipo:"Cafeterías");
        yield CargadosPI(actual.talleres, null ,actual.talleresCargados,true,actual.restaurantesCargados,cafeterias);
      }

    if(event is CargarRestaurante)
    {
      CargadosPI actual = currentState;
      List<PuntoInteres> restaurantes =await  repo.getPorTipo(tipo:"Restaurante");
      
      yield CargadosPI(actual.talleres, null ,actual.talleresCargados,actual.cafeteriasCargadas,true,actual.cafeterias, restaurantes);
    }
    if(event is SeleccionarPi)
      {

          CargadosPI actual = currentState;
          yield CargadosPI(actual.talleres, event.seleccionado,actual.talleresCargados,actual.cafeteriasCargadas,actual.restaurantesCargados,actual.cafeterias, actual.restaurantes);

      }

    if(event is IrPi)
      {
        PiState actual = currentState;
        Ruta ruta;
        bool ocurrioError = false;
        try {

          yield CargandoNavegar();

          PuntoInteres pi = event.pi;
          LocationData data = await location.getLocation().timeout(Duration(seconds: 5,), onTimeout: (){throw Exception();}).catchError((error) {
            throw Exception("Ocurrio un problema encontrando tu ubicación, revisa la conexión de internet");
          });
          Lugar miubicacion = Lugar(nombre: "Mi ubicacion",
              ubicacion: LatLng(data.latitude, data.longitude));
          print("antes coord");
          Coordinates coordPi = Coordinates(
              pi.coordenadas.latitude, pi.coordenadas.longitude);
          Lugar puntoInteres = Lugar(nombre: pi.nombre,
              ubicacion: LatLng(
                  pi.coordenadas.latitude, pi.coordenadas.longitude));
          print("1");

          ruta = await repoRutas.calcularRuta(
              Coordinates(data.latitude, data.longitude), coordPi, true).catchError((error){
            throw Exception("No se logró encontrar la ruta, revisa la conexión de internet");
          });

          print("2");

          if(ruta == null || ruta.coordenadas == null)
            {
              throw Exception("Ocurrio un problema con la conexión");
            }
          ruta.puntoSalida = miubicacion;
          ruta.puntoFinal = puntoInteres;
          ruta.conParada = false;
        }catch(error){

          ocurrioError = true;
          yield(PiError(error.toString().split(":")[1]));
        }
        if(!ocurrioError)
          {
            print("entra a navegar");
            yield(NavegarPI(ruta, actual));
          }

      }


    if(event is RebuildPi)
      {
        NavegarPI actual = currentState;
        if(currentState is NavegarPI)
          {
            yield actual.anteriorEstado;
          }

      }

    if(event is EnviarCalificacion)
      {



      }

  }
}
