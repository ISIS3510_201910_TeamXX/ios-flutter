import 'package:ciclap/Interfaz/widgets.dart';
import 'package:flutter/material.dart';

const Color morado = Color(0xFF5F0F40);

class Ayuda extends StatefulWidget {
  @override
  _AyudaState createState() => _AyudaState();
}

class _AyudaState extends State<Ayuda> {
  bool sendReport = false;

  _setnMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text("Mensaje enviado"),
            content:
                new Text("Su mensaje ha sido enviado, gracias por ayudarnos!"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Cerrar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget([
        Icon(
          Icons.help,
          size: 50,
          color: Colors.grey,
        ),
        Icon(
          Icons.warning,
          size: 50,
          color: Colors.grey,
        )
      ], "Ayuda"),
      body: !sendReport
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 20),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Icon(
                      Icons.warning,
                      color: Colors.red,
                      size: 40,
                    ),
                    Text(
                      "Reportar un problema:",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w400),
                    ),
                    InkWell(
                      onTap: () => setState(() {
                        sendReport = !sendReport;
                      }),
                      child: Icon(
                        Icons.arrow_forward,
                        color: morado,
                        size: 60,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 120),
                    )
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 30),
                ),
                HelpItem(
                    Icons.settings,
                    "¿Como hago para guardar mis configuraciones?",
                    "Al final de cada viaje no olvides calificar el nivel de dificultad del tramo  que recorriste y la comodidad que tuviste."),
                HelpItem(
                    Icons.share,
                    "¿Como comparto mis rutas guardadas con mis amigos?",
                    "Al final de cada viaje no olvides calificar el nivel de dificultad del tramo  que recorriste y la comodidad que tuviste."),
                HelpItem(
                    Icons.settings,
                    "¿Como hago para guardar mis configuraciones?",
                    "Al final de cada viaje no olvides calificar el nivel de dificultad del tramo  que recorriste y la comodidad que tuviste."),
                HelpItem(
                    Icons.share,
                    "¿Como hago para guardar mis configuraciones?",
                    "Al final de cada viaje no olvides calificar el nivel de dificultad del tramo  que recorriste y la comodidad que tuviste.")
              ],
            )
          : Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 50),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "¿Qué pasó?",
                      style: TextStyle(fontSize: 25),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 50),
                  ),
                  Center(
                    child: Container(
                      height: 200,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: amarillo)),
                      width: MediaQuery.of(context).size.width * 0.90,
                      padding: EdgeInsets.all(10.0),
                      child: new ConstrainedBox(
                        constraints: BoxConstraints(
                          maxHeight: 200.0,
                        ),
                        child: new Scrollbar(
                          child: new SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            reverse: true,
                            child: SizedBox(
                              height: 190.0,
                              child: new TextField(
                                maxLines: 100,
                                decoration: new InputDecoration(
                                  border: InputBorder.none,
                                  hintText:
                                      'Por favor cuentanos el inconveniete que tuviste',
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: SizedBox(height: 30),
                  ),
                  RaisedButton(
                    color: amarillo,
                    onPressed: () => _setnMessage(),
                    child: const Text('ENVIAR',
                        style: TextStyle(color: Colors.white, fontSize: 20)),
                  ),
                ],
              ),
            ),
    );
  }
}
