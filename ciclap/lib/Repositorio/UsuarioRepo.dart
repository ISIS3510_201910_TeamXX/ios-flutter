

import 'package:ciclap/VOs/UsuarioVO.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UsuarioRepo{



  final Firestore _db = Firestore.instance;
  final String path;
  CollectionReference ref;

  UsuarioRepo(this.path){
    ref = _db.collection(path);
  }

  Future<Usuario> getUsuario(String id) async
  {
    DocumentSnapshot snap = await ref.document(id).get();
    print("Datos "+snap.data.toString());
    return Usuario.fromMap(snap.data,id);

  }


  Future<void> postUsuario(String id, Usuario usuario) async
  {
    usuario.id = id;
    _db.collection(path).document(id).setData(usuario.toJson());

  }





}


