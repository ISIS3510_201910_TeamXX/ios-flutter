import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:ciclap/Repositorio/AutenticacionRepo.dart';
import 'package:ciclap/Repositorio/UsuarioRepo.dart';
import '../bloc.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  AutenticacionRepo _userRepository;

  LoginBloc({
    @required AutenticacionRepo userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  LoginState get initialState => LoginState.empty();



  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
   if (event is LoginWithCredentialsPressed) {
      yield* _mapLoginWithCredentialsPressedToState(
        email: event.email,
        password: event.password,
      );
    }
  }


  Stream<LoginState> _mapLoginWithGooglePressedToState() async* {
    try {
      await _userRepository.signInConGoogle();
      yield LoginState.success();
    } catch (_) {
      yield LoginState.failure();
    }
  }

  Stream<LoginState> _mapLoginWithCredentialsPressedToState({
    String email,
    String password,
  }) async* {



    yield LoginState.loading();
    try {
      await _userRepository.signInWithCredentials(email.trim(), password);
      yield LoginState.success();
    } catch (_) {
      yield LoginState.failure();
    }
  }
}
