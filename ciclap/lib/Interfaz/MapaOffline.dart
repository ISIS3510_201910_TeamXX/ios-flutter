import 'dart:collection';

import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geopoint/geopoint.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' as prefix0;
import 'package:livemap/livemap.dart';
import 'package:latlong/latlong.dart';


const Color amarillo =   Color(0xFFFBBC05);
const Color morado = Color(0xFF5F0F40);

class Mapa extends StatefulWidget {


  double long;
  double lat;

  Mapa({this.long,this.lat});
  @override
  _MapaState createState() => _MapaState();


}

class _MapaState extends State<Mapa> {

  MapController mapController;
  LiveMapController liveMapController;
  VerTerrenoBloc terrernoBloc;

  LatLng coordenadas()
  {
    if(widget.long == null || widget.lat == null)
      {
        //ubicación bogotá
        return LatLng(4.6097100, -74.0817500);
      }

    return LatLng(widget.lat, widget.long);
  }


  _MapaState() {


    mapController = MapController();
    liveMapController = LiveMapController(
        mapController:   mapController,
        autoCenter: false);
  }

  @override
  void dispose() {

    liveMapController.dispose();
    super.dispose();
  }



  LatLng _traducir(prefix0.LatLng otro)
  {
    return LatLng(otro.latitude, otro.longitude);
  }

  void dibujarRuta(Ruta ruta)
  {

    print("Punto inicial: ${ruta.puntoSalida.ubicacion} Punto final: ${ruta.puntoFinal.ubicacion}");
    Marker markerInicio = Marker(point: _traducir(ruta.puntoSalida.ubicacion)  ,builder: (context){return Icon(Icons.location_on, color: morado, size: 24,);});
    Marker markerFinal = Marker(point: _traducir(ruta.puntoFinal.ubicacion),builder: (context){return Icon(Icons.location_on, color: morado, size: 24,);} );

    Map<String,Marker> markers;
    if(ruta.conParada)
      {
        Marker markerParada = Marker(point: _traducir(ruta.puntoParada.ubicacion),builder: (context){return Icon(Icons.location_on, color: amarillo, size: 24,);} );
        markers = {"Inicio":markerInicio,"Parada":markerParada ,"Final":markerFinal};
      }else{
      markers = {"Inicio":markerInicio, "Final":markerFinal};
    }



    liveMapController.addMarkers(markers: markers);
    var lista = ruta.coordenadas;

        List<LatLng> listaLatLng = lista.map((value) => LatLng(value.latitude,value.longitude)).toList();
        liveMapController.addLine(name: "Ruta", points: listaLatLng, color: Colors.blueAccent);


    
  }

  
  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: BlocProvider.of<VerTerrenoBloc>(context),
      listener: (BuildContext context, VerTerrenoState estado) //Toca poner un if acá para cuando sea en el otro mapa
      {
          if(estado is RutasCargando){
              Ruta ruta = estado.ruta;
              dibujarRuta(ruta);

          }
      },
      child: LiveMap(
        mapController: mapController,
        liveMapController: liveMapController,
        mapOptions: MapOptions(
            center: LatLng(4.6700440450808172,-74.090612293515619),
          swPanBoundary: LatLng(4.650632, -74.228727),
          nePanBoundary: LatLng(4.7627211031767409, -74.011930),
          minZoom: 11,
          maxZoom: 15,
          zoom: 11,

        ),titleLayer: TileLayerOptions(
        tileProvider:MBTilesImageProvider.fromAsset("images/tiles.mbtiles") ,
       // urlTemplate: "images/Tiles/{z}/{x}/{y}.png",
        tms: true,
      ),

        ),
    );
  }
}
