import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:ciclap/Repositorio/UsuarioRepo.dart';
import 'package:ciclap/VOs/UsuarioVO.dart';
import '../bloc.dart';

class CuentaBloc extends Bloc<CuentaEvent, CuentaState> {

  UsuarioRepo repo = UsuarioRepo("usuarios");
  @override
  CuentaState get initialState => InitialCuentaState();

  @override
  Stream<CuentaState> mapEventToState(
    CuentaEvent event,
  ) async* {

    if(event is CargarCuenta)
      {
        yield CuentaCargando();

        Usuario usuario = await repo.getUsuario(event.uId).catchError((error){print(error);});

        yield CuentaCargada(usuario);
      }





  }
}
