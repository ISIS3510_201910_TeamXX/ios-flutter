import 'package:ciclap/VOs/RutaVO.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class VerTerrenoEvent extends Equatable {
  VerTerrenoEvent([List props = const[]]): super(props);
}



class CargarRuta extends VerTerrenoEvent
{
  final Ruta ruta;
  
  CargarRuta(this.ruta):super([ruta]);


}

class RutaCargada extends VerTerrenoEvent{

}


class Navegar extends VerTerrenoEvent{

  final Ruta ruta;
  final Widget mapa;

  Navegar(this.ruta, this.mapa);

}


class setiarState extends VerTerrenoEvent
{

}


