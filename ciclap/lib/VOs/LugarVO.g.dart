// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'LugarVO.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LugarAdapter extends TypeAdapter<Lugar> {
  @override
  Lugar read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Lugar(
      nombre: fields[0] as String,
    )
      ..nombreCompleto = fields[1] as String
      ..latitud = fields[2] as double
      ..longitud = fields[3] as double;
  }

  @override
  void write(BinaryWriter writer, Lugar obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.nombre)
      ..writeByte(1)
      ..write(obj.nombreCompleto)
      ..writeByte(2)
      ..write(obj.latitud)
      ..writeByte(3)
      ..write(obj.longitud);
  }
}
