import 'dart:convert';

import 'package:ciclap/Interfaz/Ayuda.dart';
import 'package:ciclap/Interfaz/Hisotrial.dart';
import 'package:ciclap/Repositorio/RutaRepo.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' as c;
import 'package:flutter/material.dart';
import 'package:ciclap/Interfaz/widgets.dart';
import 'package:ciclap/Interfaz/PonerDestino.dart';
import 'package:ciclap/Interfaz/Cuenta.dart';
import 'package:ciclap/Interfaz/PuntosInteres.dart';
import 'package:hive/hive.dart';
import 'package:ciclap/Interfaz/MisRutas.dart';
import 'package:ciclap/Interfaz/Comunidad.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ciclap/Logica/bloc.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:http/http.dart' as http;


const Color  negro = Color(0xFF353531);
const Color amarillo = Color(0xFFFBBC05);
const Color morado = Color(0xFF5F0F40);



class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);



  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  initState()
  {

    Hive.openBox("rutas");
    super.initState();

  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: OfflineBuilder(
        connectivityBuilder: (
            BuildContext context,
            ConnectivityResult connectivity,
            Widget child,
            ){
          final bool connected = connectivity != ConnectivityResult.none;


          return Stack(
            children: <Widget>[
              bodyBuilder(),
              Positioned(
                top: 0,
                child: Visibility(
                  visible: !connected,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height*0.15,
                    decoration: BoxDecoration(color: Color(0xFFFFABAB), boxShadow: [new BoxShadow(color: Colors.grey, blurRadius: 6.0)]),
                    child: Column(

                      children: <Widget>[
                        Expanded(child: Container()),
                        Text("Sin conexión a internet", style: TextStyle(color: Color(0xFF814369), fontSize: 20),),
                        SizedBox(height: 5),
                        Text("Algunas funcionalidades no estarán dispobiles", style: TextStyle(color: Color(0xFF814369), fontSize: 15),),
                        SizedBox(height: MediaQuery.of(context).size.height*0.03,),

                      ],
                    ),
                  ),
                ),
              ),
              Positioned(child: Visibility(visible: !connected,child: Icon(Icons.signal_wifi_off,size: 30,color: Color(0xFF4B4B4B),)), right: 20,top: MediaQuery.of(context).size.height*0.07),


            ],

          );

        },
        child: Container(),
      ),
      //-------------------------Drawer-----------------
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              padding: EdgeInsets.zero,
              child: Center(
                child: CircleAvatar(
                  radius: 50,
                  backgroundColor: Colors.white,
                ),
              ),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Color(0xffF1AF47), Color(0xffEBA86F)])),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                BlocBuilder(
                  bloc: BlocProvider.of<AutenticacionBloc>(context),
                  builder: (context, AutenticacionState state) {

                    Authenticated estado = state;

                    return Text(

                      estado.usuario.nombre,
                      style: TextStyle(fontSize: 25),
                    );
                  }
                ),
                Divider(
                  color: amarillo,
                  endIndent: MediaQuery.of(context).size.width * 0.08,
                  indent: MediaQuery.of(context).size.width * 0.08,
                ),
                FlatButton(
                  child: TextoIcono(
                    texto: "Cuenta",
                    asset: "images/cuenta.png",
                  ),
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Cuenta())),
                ),
                /* FlatButton(
                  child: TextoIcono(
                    texto: "Mis Rutas",
                    asset: "images/biciSideBar.png",
                  ),
                  onPressed: () async {
                    RutaRepo repo = RutaRepo();
                    var response = await http.get("https://my.api.mockaroo.com/rutas.json?generate=100", headers: {"X-API-Key":"cd2c01a0" });
                    List<dynamic> listaRutas = json.decode(response.body);
                    print(response.body);
                    listaRutas.forEach((value) {repo.postRuta(Ruta.fromMap(value), "667oybZ1kGlTwwzfDGFV");});
                  }
                ),*/
                FlatButton(
                  child: TextoIcono(
                    texto: "Historial",
                    asset: "images/historialSideBar.png",
                  ),
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Historial())),
                ),

                /*FlatButton(
                  child: TextoIcono(
                    texto: "Comunidad",
                    asset: "images/comunidadSideBar.png",
                  ),
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Comunidad())),
                ),
                FlatButton(
                  child: TextoIcono(
                    texto: "Configuración",
                    asset: "images/configuracionSideBar.png",
                  ),
                  onPressed: () => null,
                ),*/
                FlatButton(
                  child: TextoIcono(
                    texto: "Ayuda",
                    asset: "images/ayudaSideBar.png",
                  ),
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Ayuda())),
                ),
                SizedBox(height: MediaQuery.of(context).size.height *0.3,),
                FlatButton(
                  child: Text(
                    "Cerrar Sesión",
                    style:TextStyle(fontSize: 25, color: Colors.grey, fontWeight: FontWeight.w300),
                  ),
                  onPressed: () {
                    BlocProvider.of<AutenticacionBloc>(context).dispatch(LoggedOut());

                  },
                ),

              ],
            )
          ],
        ),
      ),

      // This trailing comma makes auto-formatting nicer for build methods.
    );


  }

  Widget bodyBuilder(){
    return  OfflineBuilder(
      child: Container(),
      connectivityBuilder:(
          BuildContext context,
          ConnectivityResult connectivity,
          Widget child,
          ){
        final bool connected = connectivity != ConnectivityResult.none;
        return Builder(
            builder: (context) => Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          FlatButton(
                            child: Icon(
                              Icons.dehaze,
                              color: negro,
                              size: 35,
                            ),
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            disabledColor: Colors.grey,
                            onPressed: connected ? () => Scaffold.of(context).openDrawer():null,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.1,
                          ),
                          Center(child: Image.asset('images/logoAmarillo.png')),
                        ],
                      ),
                    )),

                FlatButton(
                    padding: EdgeInsets.zero,
                    splashColor: Colors.transparent,
                    disabledColor: Color(0xFFF1F1F1),
                    onPressed: connected ?() {
                      Navigator.push(context,MaterialPageRoute(builder: (context) => PonerDestino()),
                      );
                    }:null,
                    child: Container(
                        decoration: BoxDecoration(
                            border: Border(
                                top: BorderSide(color: Colors.grey),
                                bottom: BorderSide(color: Colors.grey))),
                        height: 55,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width * 0.1,
                            ),
                            Image.asset('images/search.png')
                          ],
                        ))),
                //---------------END PARTE DE ARRIBA----------------------

                SizedBox(height: MediaQuery.of(context).size.height *0.09,),

                //-----------------FAVORITOS + Añadir--------------------
/*            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.05,
                  vertical: MediaQuery.of(context).size.width * 0.05),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Favoritos",
                    style: TextStyle(fontSize: 20),
                  ),

                ],
              ),
            ),
            Divider(
              color: amarillo,
              height: 0,
            ),

            //----------------------Para poner en el list builder------------
            Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextoIcono(texto: "Casa", asset: "images/casaIcon.png"),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text("AUN NO IMPLEMENTADA",
                        style: TextStyle(
                            fontFamily: "Roboto",
                            fontSize: 11,
                            color: Colors.grey)),
                  ),
                ],
              ),
            ),
            Divider(
              indent: 10,
              endIndent: 10,
            ),*/

                //----------------------Para poner en el list builder------------

                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.05,
                      vertical: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Mis rutas",
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                ),

                FutureBuilder(
                  future: Hive.openBox("rutas"),
                  builder: (BuildContext context, AsyncSnapshot snap)
                  {
                    if(snap.connectionState == ConnectionState.done)
                    {
                      if(snap.hasError){
                        return const Text("Error");
                      }else{
                        final rutas = Hive.box("rutas") ;
                        if(rutas.isEmpty)
                        {
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20),
                            child: Center(child: Image.asset("images/NoHayRutas.png"),),
                          );
                        }
                        return Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: grisSombra, width: 1),
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                              color: Colors.white,
                              boxShadow: [const BoxShadow(color: grisClaro, blurRadius: 5.0)],
                            ),
                          width: MediaQuery.of(context).size.width*0.95,
                          height: MediaQuery.of(context).size.height*0.47,

                          child: ListView.builder(itemCount: rutas.length,itemBuilder: (BuildContext context, int index){
                            final ruta = rutas.values.toList()[index] as Ruta;
                            if(ruta!=null)
                            {
                              return Column(
                                children: <Widget>[
                                  Rutas(ruta),
                                  Divider()
                                ],
                              );
                            }
                            return Container();
                          }),
                        );
                      }

                    }else{
                      return CupertinoActivityIndicator();
                    }
                  },
                ),

                Expanded(child: Container()),
                 Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: grisSombra, width: 1),
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Colors.white,
                      boxShadow: [const BoxShadow(color: grisClaro, blurRadius: 5.0)],
                    ),
                    width: MediaQuery.of(context).size.width*0.95,
                    height: MediaQuery.of(context).size.height*0.12,
                    child: Material(
                      color: Colors.white,
                      child: InkWell(
                        onTap: (){Navigator.push(context,
                            MaterialPageRoute(builder: (context) => PuntosInteres()));},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: const Text("Puntos de interés", style: TextStyle(fontSize: 26),),
                                ),
                                const Text("Restaurantes | Talleres | Cafeterías",style: TextStyle(color: Color(0xFF5B5B5B), fontSize: 13),)
                              ],
                            ),
                            Icon(Icons.location_city, color: Color(0xFFBB8DAA),size: 35,)

                          ],
                        ),
                      ),
                    )
                  ),


                SizedBox(height: 35,),
              ],
            ));},
    );



  }
}