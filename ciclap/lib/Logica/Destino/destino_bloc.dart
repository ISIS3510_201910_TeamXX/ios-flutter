import 'dart:async';
import 'dart:core';
import 'dart:core' as prefix0;
import 'package:bloc/bloc.dart';
import 'package:ciclap/Repositorio/LugarRepo.dart';
import 'package:ciclap/Repositorio/RutaRepo.dart';
import 'package:ciclap/VOs/LugarVO.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


import 'package:location/location.dart';
import '../bloc.dart';


class DestinoBloc extends Bloc<DestinoEvent, DestinoState> {

  final RutaRepo rutaRepo = RutaRepo();
  final Location location = Location();

  Lugar lugarParada;
  Lugar lugarDestino;
  Lugar lugarInicio;


  @override
  DestinoState get initialState => InitialDestinoState(false);

  @override
  Stream<DestinoState> mapEventToState(
      DestinoEvent event,
      ) async* {

    if (event is Cargar){
      LugarRepo repo = LugarRepo();
      List<Lugar> lugares = await repo.getLugaresRecientes();

      yield RecientesCargados(lugares, currentState.conParada);
    }

    if(event is AgregarParada)
    {
      if(currentState is RecientesCargados)
      {
        RecientesCargados actual = currentState;
        yield RecientesCargados(actual.lugares,!currentState.conParada);
      }
      else if(currentState is SeleccionadaUbicacion){
        SeleccionadaUbicacion actual = currentState;
        yield SeleccionadaUbicacion(!currentState.conParada,actual.lugares,actual.lugarDestino,actual.miUbicacion );
      }
    }

    if(event is SeleccionarReciente)
    {

      if(currentState is SeleccionadaUbicacion )
      {
        SeleccionadaUbicacion actual = currentState;

        yield SeleccionadaUbicacion(actual.conParada, actual.lugares, event.lugar,  actual.miUbicacion);

      }else{

        RecientesCargados actual = currentState;

        SeleccionarReciente evento = event;

        yield SeleccionadaUbicacion(actual.conParada, actual.lugares, evento.lugar, null);
      }

    }

    if(event is SeleccionarMiUbicacion)
    {
      Lugar miUbicacion = Lugar();


      if(currentState is SeleccionadaUbicacion )
      {
        SeleccionadaUbicacion actual = currentState;


        LocationData data = await location.getLocation().catchError((error){throw Exception("Ocurrio un problema encontrando tu ubicación");});
        miUbicacion.nombre = "Mi ubicación";
        miUbicacion.ubicacion = LatLng(data.latitude, data.longitude);
        print("primer if"+ data.longitude.toString());

        yield SeleccionadaUbicacion(actual.conParada, actual.lugares, actual.lugarDestino,  miUbicacion);

      }else{

        RecientesCargados actual = currentState;
        SeleccionarMiUbicacion evento = event;

        LocationData ubicacion = await location.getLocation();
        miUbicacion.nombre = "Mi ubicación";
        miUbicacion.ubicacion = LatLng(ubicacion.latitude, ubicacion.longitude);

        print("Ubicación en else "+ ubicacion.longitude.toString() + " " + ubicacion.latitude.toString());

        yield SeleccionadaUbicacion(actual.conParada, actual.lugares, null,miUbicacion );
      }
    }

    if(event is EnviarDestino)
    {
      print("enviardestino1");
      yield CargandoRuta(currentState.conParada, currentState);
      Lugar origen = event.lugarOrigen;
      Lugar destino = event.lugarDestino;

      Coordinates puntoInicial;
      Coordinates puntoFinal;
      Ruta ruta;
      try{


        if(lugarInicio != null)
          {
            puntoInicial = Coordinates(lugarInicio.ubicacion.latitude, lugarInicio.ubicacion.longitude);
            origen = lugarInicio;
          }else
            {
              if(origen.ubicacion == null)
              {

                List<Address> direcciones = await Geocoder.local.findAddressesFromQuery(origen.nombre).timeout(Duration(seconds: 6)).catchError((error){
                  if(error is PlatformException)
                  {
                    throw Exception("No se puede encontrar la ubicación de origen.\n Pruebe con otra o escribiendo Bogotá al final de la búsqueda");
                  }else{
                    throw Exception("Algo salió mal con la conexión");
                  }
                });
                var first = direcciones.first;
                puntoInicial = first.coordinates;
                origen.ubicacion = LatLng(puntoInicial.latitude, puntoInicial.longitude);

              }else{

                puntoInicial = Coordinates(origen.ubicacion.latitude, origen.ubicacion.longitude);
              }
            }


        if(lugarDestino != null)
          {
            puntoFinal = Coordinates(lugarDestino.ubicacion.latitude, lugarDestino.ubicacion.longitude);
            destino = lugarDestino;
          }else{
          if(destino.ubicacion == null)
          {

            List<Address> direcciones = await Geocoder.local.findAddressesFromQuery(destino.nombre).timeout(Duration(seconds: 6)).catchError((error){
              if(error is PlatformException)
              {
                throw Exception("No se puede encontrar la ubicación de la parada. \n Pruebe con otra o escribiendo Bogotá al final de la búsqueda");
              }else{
                throw Exception("Algo salió mal con la conexión, revise su conexión a internet");
              }
            });
            var first = direcciones.first;
            puntoFinal = first.coordinates;
            destino.ubicacion = LatLng(puntoFinal.latitude, puntoFinal.longitude);
          }else{

            puntoFinal= Coordinates(destino.ubicacion.latitude, destino.ubicacion.longitude);
          }

        }



        ruta = await rutaRepo.calcularRuta(puntoInicial, puntoFinal, event.cicloruta).timeout(Duration(seconds: 6)).catchError((error){throw Exception("Ocurrió un error en la conexión");});;
        ruta.puntoSalida = origen;
        ruta.puntoFinal = destino;
        ruta.conParada = false;

      }catch(excepcion)
      {
        yield Error(excepcion.toString().split(":")[1], currentState);
      }


      yield(ListoParaEnviar(conParada: currentState.conParada, ruta: ruta, anteriorEstado: currentState));

    }

    if(event is EnviarDestinoConParada){

      print("1");
      yield CargandoRuta(currentState.conParada,currentState);
      Lugar origen = event.lugarOrigen;
      Lugar destino = event.lugarDestino;
      Lugar parada = event.lugarParada;

      Coordinates puntoInicial;
      Coordinates puntoParada;
      Coordinates puntoFinal;
      Ruta ruta;
      try{
        print("2");

        if(lugarParada != null)
          {
            puntoParada = Coordinates(lugarParada.ubicacion.latitude, lugarParada.ubicacion.longitude);
            parada = lugarParada;
          }else{

          List<Address> direcciones = await Geocoder.local.findAddressesFromQuery(parada.nombre).timeout(Duration(seconds: 5)).catchError((error){
            print("4");
            if(error is PlatformException)
            {
              throw Exception("No se puede encontrar la ubicación de la parada. \nPruebe con otra o escribiendo Bogotá al final de la búsqueda");
            }else{
              throw Exception("Algo salió mal con la conexión, revise su conexión a internet");
            }
          });


          var first = direcciones.first;
          puntoParada = first.coordinates;
          parada.ubicacion = LatLng(puntoParada.latitude, puntoParada.longitude);

        }




        if(lugarInicio != null)
          {

            puntoInicial = Coordinates(lugarInicio.ubicacion.latitude, lugarInicio.ubicacion.longitude);
            origen = lugarInicio;
          }else{

          if(origen.ubicacion == null)
          {

            List<Address> direcciones = await Geocoder.local.findAddressesFromQuery(origen.nombre).timeout(Duration(seconds: 6)).catchError((error){
              if(error is PlatformException)
              {
                throw Exception("No se puede encontrar la ubicación del origen.\n Pruebe con otra o escribiendo Bogotá al final de la búsqueda");
              }else{
                throw Exception("Algo salió mal con la conexión, revise su conexión a internet");
              }
            });
            var first = direcciones.first;
            puntoInicial = first.coordinates;
            origen.ubicacion = LatLng(puntoInicial.latitude, puntoInicial.longitude);
          }else{

            puntoInicial = Coordinates(origen.ubicacion.latitude, origen.ubicacion.longitude);
            print("NombreInicial ${origen.nombre}" );
          }
        }


        if(lugarDestino != null)
          {
            puntoFinal = Coordinates(lugarDestino.ubicacion.latitude, lugarDestino.ubicacion.longitude);
            destino = lugarDestino;
          }else{

          if(destino.ubicacion == null)
          {

            print("destino.nombre ${destino.nombre}");
            List<Address> direcciones = await Geocoder.local.findAddressesFromQuery(destino.nombre).timeout(Duration(seconds: 6)).catchError((error){
              if(error is PlatformException)
              {
                throw Exception("No se puede encontrar la ubicación del destino. \nPruebe con otra o escribiendo Bogotá al final de la búsqueda");
              }else{
                throw Exception("Algo salió mal con la conexión, revise su conexión a internet");
              }
            });
            var first = direcciones.first;
            puntoFinal = first.coordinates;
            destino.ubicacion = LatLng(puntoFinal.latitude, puntoFinal.longitude);
          }else{
            print("segundo else");
            print("NombreFinal ${destino.nombre}" );
            puntoFinal= Coordinates(destino.ubicacion.latitude, destino.ubicacion.longitude);
          }
        }




        ruta = await rutaRepo.calcularRutaConParada(puntoInicial, puntoFinal, puntoParada, event.cicloruta).timeout(Duration(seconds: 7)).catchError((error){print(error);throw Exception("Ocurrió un error en la conexión");});
        ruta.puntoFinal = destino;
        ruta.puntoSalida = origen;
        ruta.puntoParada = parada;
        ruta.conParada = true;

      }catch(error){
        print("5");
        yield Error(error.toString().split(":")[1], currentState);
      }

      yield(ListoParaEnviar(conParada: currentState.conParada, ruta: ruta, anteriorEstado: currentState));

    }//END Event

    if(event is RebuildSeleccionada)
    {
      yield event.anteriorEstado;
    }


    if(event is SeleccionarDeSuggestion)
      {
        print("eNTRA A BLOC SELECCIONARSUGGESTION");

        switch (event.tipoLugar){
          case "inicio":
            lugarInicio = event.lugar;
            break;

          case "parada":
            lugarParada = event.lugar;
            break;

          case "destino":
            lugarDestino = event.lugar;
            break;
        }


      }


  }//END mapEventtoState



}
