

import 'package:hive/hive.dart';

part 'InstruccionVO.g.dart';

@HiveType()
class Instruccion
{

  @HiveField(0)
  double distance;
  @HiveField(1)
  double duration;
  @HiveField(2)
  double type;
  @HiveField(3)
  String instruction;
  @HiveField(4)
  String name;

  List<dynamic> way_points;

  Instruccion({this.instruction,this.duration,this.distance,this.name,this.type,this.way_points});


  Instruccion.fromMap(Map json):

    distance = json['distance'].toDouble() ?? 0,
    duration = json['duration'].toDouble() ?? 0,
    type = json['type'].toDouble() ?? 0,
    instruction = json['instruction']?? "",
    name = json['name'] ?? "",
        way_points = json["way_points"];



}