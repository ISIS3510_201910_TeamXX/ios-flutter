import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

@immutable
abstract class NavegarState extends Equatable{
  NavegarState([List props = const[]]): super(props);
}
  
class InitialNavegarState extends NavegarState {}


class InsCargada extends NavegarState{

  final String instruccion;
  final String name;
  final Widget widget;


  InsCargada(this.instruccion, this.name, this.widget):super([instruccion,widget]);

}


class AgregandoSticker extends NavegarState{

  final String instruccion;
  final String name;
  final Widget widget;
  String sel;

  String stickerSeleccionado;

  AgregandoSticker(this.instruccion, this.name, this.widget,this.sel):super([instruccion,widget,sel]);
}








