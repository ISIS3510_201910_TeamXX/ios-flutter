import 'dart:async';
import 'dart:io';

import 'package:ciclap/Repositorio/PIrepo.dart';
import 'package:ciclap/VOs/PuntoInteresVO.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:ciclap/Interfaz/widgets.dart';
import 'package:flutter/services.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

class AgregarPI extends StatefulWidget {
  @override
  _AgregarPIState createState() => _AgregarPIState();
}

class _AgregarPIState extends State<AgregarPI> {

  Completer<GoogleMapController> _controlador = Completer();
  final _formKey = GlobalKey<FormState>();
  TextEditingController controllerNombre = TextEditingController();
  TextEditingController controllerDireccion = TextEditingController();
  TextEditingController controllerTelefono = TextEditingController();
  TextEditingController controllerDescripcion = TextEditingController();
  LatLng ubicacionPi;
  PIrepo repo = PIrepo("puntosInteres");
  String dropdownValue;
  Set<Marker> _markers;


  @override
  void initState() {

    super.initState();
    _markers = Set.from([]);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,

      appBar: AppBarWidget(
        [
          Icon(
            Icons.history,
            size: 1,
            color: Colors.transparent,
          )
        ],
        "Agregar Punto",
      ),
      body: Column(
        children: <Widget>[

          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.30,
            child: Stack(

              children: <Widget>[
                GoogleMap(myLocationEnabled: true,
                  markers: _markers,
                  initialCameraPosition: CameraPosition(
                      target: LatLng(4.6097100, -74.0817500), zoom: 11),
                  onMapCreated: (GoogleMapController controller) {
                    _controlador.complete(controller);
                  },
                  onTap: (pos){
                    print(pos);

                  setState(() {
                    _markers.add(Marker(markerId: MarkerId('1'), icon: BitmapDescriptor.defaultMarker, position: pos),);
                    ubicacionPi = pos;
                  });

                  },
                ),

                Align(alignment: Alignment.bottomRight,
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Text("Presiona en la ubicacion"),
                      color: Colors.white,
                      
                    )
                )
              ],
            ),
          ),
          Builder(
            builder: (context) {
              return Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.55,
                    child: ListView(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 5),
                              child: TextFormField(
                                controller: controllerNombre,
                                decoration: InputDecoration(
                                    hintText: "Nombre Lugar", icon: Icon(
                                    Icons.import_contacts)),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor ingrese un valor';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 5),
                              child: TextFormField(
                                controller: controllerDireccion,
                                decoration: InputDecoration(
                                    hintText: "Direccion", icon: Icon(
                                    Icons.location_on)),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor ingrese un valor';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 5),
                              child: TextFormField(
                                controller: controllerTelefono,
                                keyboardType: TextInputType.phone,
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly
                                ],
                                decoration: InputDecoration(
                                    hintText: "Telefono", icon: Icon(Icons.phone)),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor ingrese un valor';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 5),
                              child: TextFormField(
                                controller: controllerDescripcion,
                                decoration: InputDecoration(
                                    hintText: "Descripción", icon: Icon(Icons.edit)),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor ingrese un valor';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 15),
                              child: DropdownButton<String>(
                                hint: Text("Categoría del punto de interés"),
                                isExpanded: true,
                                value: dropdownValue,
                                icon: Icon(Icons.arrow_drop_down),
                                iconSize: 24,
                                elevation: 16,
                                style: TextStyle(color: Colors.black54, fontSize: 16),
                                underline: Container(
                                  height: 2,
                                  color: Colors.grey,
                                ),
                                onChanged: (String newValue) {
                                  setState(() {
                                    dropdownValue = newValue;
                                  });
                                },
                                items: <String>[
                                  'Taller',
                                  'Cafeterías',
                                  'Restaurante',
                                ]
                                    .map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                            ),
                            SizedBox(height: MediaQuery
                                .of(context)
                                .size
                                .height * 0.09,),
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 5.0),
                              child: BotonPrimario(
                                color: amarillo,
                                texto: "  Enviar  ",
                                onPressed: () async {
                                  // Validate returns true if the form is valid, or false
                                  // otherwise.

                                  if (ubicacionPi == null) {
                                    Scaffold.of(context).showSnackBar(SnackBar(
                                        content: Text(
                                            "Por favor elija la ubicación")));
                                  }

                                  if (dropdownValue == null) {
                                    Scaffold.of(context).showSnackBar(SnackBar(
                                        content: Text(
                                            "Por favor elija la categoría")));
                                  }
                                  else if (_formKey.currentState.validate() && ubicacionPi != null) {
                                    // If the form is valid, display a Snackbar.
                                    String nombre = controllerNombre.text;
                                    String direccion = controllerDireccion.text;

                                    int telefono = int.parse(
                                        controllerTelefono.text);
                                    String descripcion = controllerDescripcion.text;
                                    String categoria = dropdownValue;


                                    GeoPoint puntoPI = GeoPoint(
                                        ubicacionPi.latitude, ubicacionPi.longitude);
                                    Scaffold.of(context)
                                        ..hideCurrentSnackBar()
                                        ..showSnackBar(
                                        SnackBar(content: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text('Procesando ...'),
                                            CircularProgressIndicator(),
                                          ],
                                        ),));


                                    await repo.postPI(PuntoInteres(puntoPI,nombre,0,0,categoria,descripcion,telefono, direccion));
                                    bool conectado = await checkConectivity();


                                    if (conectado)
                                      {
                                        print("Entra a conectado");
                                        Scaffold.of(context)..hideCurrentSnackBar()..showSnackBar(
                                              SnackBar(backgroundColor: Colors.lightGreen, content: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Text('Se ha publicado exitosamente'),
                                                  Icon(Icons.check),
                                                ],
                                              ),));
                                      }else{
                                      Scaffold.of(context)..hideCurrentSnackBar()..showSnackBar(
                                          SnackBar(content: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text('Se ha guardado para publicar cuando estés conectado'),
                                              Icon(Icons.save),
                                            ],
                                          ),));
                                    }

                                    await Future.delayed(Duration(seconds: 2));
                                    Navigator.pop(context);
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.width * 0.4,)
                      ],
                    ),
                  ),
                ),
              );
            }
          )
        ],

      ),
    );
  }


  Future<bool> checkConectivity() async
  {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    }
    return false;
  }


  @override
  void dispose() {
    controllerNombre.dispose();
    controllerDescripcion.dispose();
    controllerTelefono.dispose();
    controllerDireccion.dispose();
    super.dispose();
  }
}



