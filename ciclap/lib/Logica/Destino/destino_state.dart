import 'package:ciclap/VOs/LugarVO.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DestinoState extends Equatable {

  bool conParada;
  DestinoState([List props = const[]]):super(props);

}

class InitialDestinoState extends DestinoState {

  bool conParada = false;
  InitialDestinoState(this.conParada):super([conParada]);
}

class ConParada extends DestinoState{
  bool conParada;
  ConParada(this.conParada):super([conParada]);
}


class SeleccionadaUbicacion extends DestinoState{

  Lugar lugarDestino;
  Lugar miUbicacion;
  final List<Lugar> lugares;
  bool conParada;

  SeleccionadaUbicacion(this.conParada,this.lugares, this.lugarDestino, this.miUbicacion,):super([conParada,lugares,miUbicacion,lugarDestino]);

}

class RecientesCargados extends DestinoState{

  final List<Lugar> lugares;
  bool conParada;

  RecientesCargados(this.lugares,this.conParada):super([lugares,conParada]);

}

class CargandoRuta extends DestinoState
{

  bool conParada;
  DestinoState anteriorEstado;

  CargandoRuta(this.conParada, this.anteriorEstado):super([conParada,anteriorEstado]);
}


class ListoParaEnviar extends DestinoState
{
  bool conParada;
  Ruta ruta;
  CargandoRuta anteriorEstado;

  ListoParaEnviar({this.conParada, this.ruta, this.anteriorEstado}):super([conParada,ruta,anteriorEstado]);

}

class Error extends DestinoState
{
  String error;
  DestinoState anteriorEstado;

  Error(this.error,this.anteriorEstado):super([error,anteriorEstado]);

}





