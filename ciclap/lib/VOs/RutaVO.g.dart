// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RutaVO.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class RutaAdapter extends TypeAdapter<Ruta> {
  @override
  Ruta read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Ruta(
      nombre: fields[0] as String,
      puntoSalida: fields[1] as Lugar,
      puntoParada: fields[3] as Lugar,
      puntoFinal: fields[2] as Lugar,
      instrucciones: (fields[6] as List)?.cast<Instruccion>(),
    )
      ..conParada = fields[4] as bool
      ..puntosPersistidos = (fields[5] as List)?.cast<Lugar>()
      ..distancia = fields[7] as String
      ..duracion = fields[8] as String
      ..calorias = fields[9] as String
      .._distanciaDouble = fields[10] as double
      .._duracionDouble = fields[11] as double
      .._caloriasDouble = fields[12] as double;
  }

  @override
  void write(BinaryWriter writer, Ruta obj) {
    writer
      ..writeByte(13)
      ..writeByte(0)
      ..write(obj.nombre)
      ..writeByte(1)
      ..write(obj.puntoSalida)
      ..writeByte(2)
      ..write(obj.puntoFinal)
      ..writeByte(3)
      ..write(obj.puntoParada)
      ..writeByte(4)
      ..write(obj.conParada)
      ..writeByte(5)
      ..write(obj.puntosPersistidos)
      ..writeByte(6)
      ..write(obj.instrucciones)
      ..writeByte(7)
      ..write(obj.distancia)
      ..writeByte(8)
      ..write(obj.duracion)
      ..writeByte(9)
      ..write(obj.calorias)
      ..writeByte(10)
      ..write(obj._distanciaDouble)
      ..writeByte(11)
      ..write(obj._duracionDouble)
      ..writeByte(12)
      ..write(obj._caloriasDouble);
  }
}
