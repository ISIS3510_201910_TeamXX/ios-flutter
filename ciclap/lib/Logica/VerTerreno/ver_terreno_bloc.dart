import 'dart:async';
import 'package:bloc/bloc.dart';
import '../bloc.dart';

class VerTerrenoBloc extends Bloc<VerTerrenoEvent, VerTerrenoState> {
  @override
  VerTerrenoState get initialState => InitialVerTerrenoState();

  @override
  Stream<VerTerrenoState> mapEventToState(
    VerTerrenoEvent event,
  ) async* {

    if(event is CargarRuta)
      {
        yield RutasCargando(event.ruta);

      }

    if(event is RutaCargada)
      {

      }


    if(event is Navegar)
      {
          yield Navegando(event.ruta, event.mapa);
      }

    if(event is setiarState)
      {
        yield setEstado();

      }

  }
}
