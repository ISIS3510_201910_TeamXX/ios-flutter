import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:latlong/latlong.dart';



class PuntoInteres{

  String id;
  GeoPoint coordenadas;
  String nombre;
  String descripcion;
  String direccion;
  int telefono;
  String categoria;
  int estrellas;
  int numResenias;

  PuntoInteres(this.coordenadas, this.nombre, this.estrellas, this.numResenias, this.categoria, this.descripcion, this.telefono, this.direccion);


  PuntoInteres.fromMap(Map snapshot,String id) :
        id = id ?? '',
        nombre = snapshot['nombre'] ?? '',
        coordenadas = snapshot['coordenadas'] ?? '',
        direccion = snapshot['direccion'] ?? '',
        estrellas = snapshot['estrellas'] ?? '',
        categoria = snapshot['categoria'] ?? '',
        numResenias = snapshot['numResenias'],
        descripcion = snapshot['descripcion'] ?? '',
        telefono = snapshot['telefono']??0;

  toJson() {
    return {
      "nombre": nombre,
      "coordenadas": coordenadas,
      "direccion": direccion,
      "estrellas": estrellas,
      "categoria": categoria,
      "numResenias": numResenias,
      "descripcion" : descripcion,
      "telefono" : telefono,

    };
  }

}