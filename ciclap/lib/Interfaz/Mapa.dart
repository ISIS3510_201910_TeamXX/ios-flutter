import 'dart:async';
import 'dart:collection';

import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/Repositorio/PIrepo.dart';
import 'package:ciclap/Repositorio/RutaRepo.dart';
import 'package:ciclap/VOs/PuntoInteresVO.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

//import 'package:flutter_map/flutter_map.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';


import 'package:latlong/latlong.dart' as latlng;



const Color amarillo =   Color(0xFFFBBC05);
const Color morado = Color(0xFF5F0F40);

class GoogleMapa extends StatefulWidget {
  LatLng posSel;
  var height;
  var width;
  Ruta ruta;
  bool dibujarRuta;
  final Set<Polyline>polyline={};
  final Set<Marker> markers = {};

  Completer<GoogleMapController> _controlador = Completer();

  GoogleMapa(this.dibujarRuta);

  @override
  _GoogleMapaState createState() => _GoogleMapaState();

  Future<LatLng> getPosicion(widthMapa, heightMapa) async
  {
    GoogleMapController controller = await _controlador.future;

    return await controller.getLatLng(
        ScreenCoordinate(x: (widthMapa / 2), y: (heightMapa / 2)));
  }



}

class _GoogleMapaState extends State<GoogleMapa> {

  VerTerrenoBloc terrenoBloc;



  @override
  Widget build(BuildContext context) {


    if(widget.dibujarRuta){

      return BlocListener(
          bloc: BlocProvider.of<VerTerrenoBloc>(context),

          listener:(BuildContext contexto, VerTerrenoState estado)
          {
            bool dib = true;
            print("entra al listener");
            Ruta ruta;
            if(estado is RutasCargando || estado is Navegando )
            {

              if(estado is RutasCargando )
                {
                  print("entra a rutascargando");
                  ruta = estado.ruta;

                }
              if(estado is Navegando)
                {
                  ruta = estado.ruta;
                }
              if(estado is setEstado)
                {
                  dib = false;
                  setState(() {

                  });
                }
              if(dib)
                {
                  dibujarRuta(ruta);
                  BlocProvider.of<VerTerrenoBloc>(context).dispatch(RutaCargada());
                }

            }
          },
          child: BlocBuilder(
            bloc: BlocProvider.of<VerTerrenoBloc>(context),
            builder: (BuildContext contexto, VerTerrenoState estado)
            {
              if(estado is RutasCargando)
              {
                return  GoogleMap(myLocationEnabled: true ,polylines: widget.polyline, markers: widget.markers ,initialCameraPosition: CameraPosition(target: LatLng(4.6097100, -74.0817500),
                  zoom:11, ),onTap: (pos){
                  setState(() {
                    widget.markers.add(Marker(markerId: MarkerId('1'), icon: BitmapDescriptor.defaultMarker, position: pos),);
                    widget.posSel = pos;
                  });
                  }, onMapCreated: (GoogleMapController controller){widget._controlador.complete(controller);});


              }
                else {

                return CupertinoActivityIndicator();
              }

            },

          )
      );
    }else{
      return  BlocListener(
        listener: (BuildContext context, PiState estado){

          if(estado is CargadosPI)
          {
            dibujarPuntosInteres();
            if(estado.sel != null){

              var lista;
              switch(estado.sel.tipo)
              {
                case "Taller":
                  lista = estado.talleres;
                  break;
                case "Cafeterías":
                  lista = estado.cafeterias;
                  break;

                case "Restaurante":
                  lista = estado.restaurantes;
                  break;
              }
              print(estado.sel.sel);
              _goToLocation(lista[estado.sel.sel].coordenadas);
            }

          }

        },
        bloc: BlocProvider.of<PiBloc>(context),
        child: GoogleMap(polylines: widget.polyline, markers: widget.markers ,initialCameraPosition: CameraPosition(target: LatLng(4.6097100, -74.0817500),
          zoom:11, ), onMapCreated: (GoogleMapController controller){widget._controlador.complete(controller);},myLocationEnabled: true,),
      );
    }

  }



  Future<void> _goToLocation(GeoPoint p) async {
    print("entra a gotoLocation");
    final GoogleMapController controller = await widget._controlador.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target:geoPtoLatLang(p), zoom: 15, bearing:0,tilt: 59.440717697143555,)));

  }


  void dibujarPuntosInteres() async
  {
    List<PuntoInteres> puntos = await PIrepo("puntosInteres").getPuntosInteres();
    for(var p in puntos)
    {
      widget.markers.add(Marker(markerId: MarkerId(p.nombre), position: geoPtoLatLang(p.coordenadas), icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet)));
    }
    setState(() {});
  }


  void dibujarRuta(Ruta ruta) async
  {

    widget.markers.add(Marker(markerId: MarkerId(ruta.puntoSalida.nombre), position: ruta.puntoSalida.ubicacion?? LatLng(ruta.puntoSalida.latitud, ruta.puntoSalida.longitud), icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet)));
    widget.markers.add(Marker(markerId: MarkerId(ruta.puntoFinal.nombre), position: ruta.puntoFinal.ubicacion?? LatLng(ruta.puntoFinal.latitud, ruta.puntoFinal.longitud),icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet)));



    if(ruta.conParada)
    {
      widget.markers.add(Marker(markerId: MarkerId(ruta.puntoSalida.nombre), position: ruta.puntoParada.ubicacion, ));
    }

    widget.polyline.add(Polyline(
      polylineId: PolylineId("ruta1"),width: 4,
      visible: true,

      points: ruta.coordenadas.map((value) => LatLng(value.latitude, value.longitude)).toList(),
      color: Colors.blueAccent,

    ));

      List<Marker> lista = await RutaRepo().getMarcadores();
    widget.markers.addAll(lista);
    print("Despues de agregar markers");

    setState(() {

    });

  }



  LatLng geoPtoLatLang(GeoPoint geop)
  {
    return LatLng(geop.latitude, geop.longitude);
  }



}



