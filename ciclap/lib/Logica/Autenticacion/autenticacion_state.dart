import 'package:ciclap/VOs/UsuarioVO.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AutenticacionState extends Equatable {
  AutenticacionState([List props = const []]) : super(props);
}

class Uninitialized extends AutenticacionState {
  @override
  String toString() => 'Uninitialized';
}

class Authenticated extends AutenticacionState {

  final Usuario usuario;
  final String uid;

  Authenticated(this.usuario, this.uid) : super([ usuario, uid]);

}

class Unauthenticated extends AutenticacionState {
  @override
  String toString() => 'Unauthenticated';
}