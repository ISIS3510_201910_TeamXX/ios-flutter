import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AutenticacionEvent extends Equatable {
  AutenticacionEvent([List props = const []]) : super(props);
}

class AppStarted extends AutenticacionEvent {
  @override
  String toString() => 'AppStarted';
}

class LoggedIn extends AutenticacionEvent {
  @override
  String toString() => 'LoggedIn';
}

class LoggedOut extends AutenticacionEvent {
  @override
  String toString() => 'LoggedOut';
}