import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/VOs/PuntoInteresVO.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PiEvent extends Equatable{

  PiEvent([List props = const[]]): super(props);
}


class CargarPi extends PiEvent{

}


class SeleccionarPi extends PiEvent{
  final Seleccionado seleccionado;
  SeleccionarPi(this.seleccionado): super([seleccionado]);

}

class IrPi extends PiEvent{
  final PuntoInteres pi;

  IrPi(this.pi);

}

class CargarCafe extends PiEvent
{

}

class CargarRestaurante extends PiEvent
{

}
class RebuildPi extends PiEvent{

}

class EnviarCalificacion extends PiEvent{

  int calificacion;


  
}