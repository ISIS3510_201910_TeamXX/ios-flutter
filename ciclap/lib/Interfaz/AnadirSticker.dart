import 'package:flutter/material.dart';
import 'package:ciclap/Interfaz/widgets.dart';
import 'package:ciclap/Interfaz/Mapa.dart' as mapa;

class AnadirSticker extends StatefulWidget {
  AnadirSticker({Key key}) : super(key: key);

  _AnadirStickerState createState() => _AnadirStickerState();
}

class _AnadirStickerState extends State<AnadirSticker> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 50.0),
        child: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: Row(children: <Widget>[
              IconButton(
                onPressed: () => Navigator.pop(context),
                icon: Icon(Icons.close),
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white, border: Border.all(color: amarillo)),
                width: MediaQuery.of(context).size.width * 0.75,
                padding: EdgeInsets.all(10.0),
                child: new TextFormField(
                  maxLines: 1,
                  enabled: true,
                  initialValue: "¿Dónde?",
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                  ),
                ),
              ),
            ]),
          ),
          Stack(alignment: Alignment.topCenter, children: <Widget>[
            Positioned(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.95,
                height: MediaQuery.of(context).size.height * 0.35,
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.black)),
                child: mapa.GoogleMapa(true),
              ),
            ),
            Positioned(
              top: 50,
              child: Icon(
                Icons.location_on,
                color: morado,
                size: 100,
              ),
            )
          ]),
          Center(
            child: Container(
              margin: EdgeInsets.only(top: 20),
              height: 150,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: amarillo, width: 2)),
              width: MediaQuery.of(context).size.width * 0.90,
              padding: EdgeInsets.all(10.0),
              child: new ConstrainedBox(
                constraints: BoxConstraints(),
                child: new Scrollbar(
                  child: new SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    reverse: true,
                    child: SizedBox(
                      child: new TextField(
                        maxLines: 100,
                        decoration: new InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30.0),
            child: ButtonTheme(
              height: 60,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                  side: BorderSide(
                      color: Colors.black, style: BorderStyle.solid)),
              minWidth: MediaQuery.of(context).size.width * 0.65,
              child: FlatButton.icon(
                icon: Icon(
                  Icons.camera_alt,
                  color: morado,
                  size: 50,
                ),
                color: Colors.white,
                onPressed: () => {},
                label: Text(
                  'Añadir foto',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                      fontWeight: FontWeight.w300),
                ),
              ),
            ),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: RaisedButton(
                color: amarillo,
                onPressed: () => {},
                child: const Text('ENVIAR',
                    style: TextStyle(color: Colors.white, fontSize: 20)),
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
