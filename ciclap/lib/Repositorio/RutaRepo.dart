
import 'dart:io';
import 'package:ciclap/VOs/InstruccionVO.dart';
import 'package:ciclap/VOs/LugarVO.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:cloud_firestore/cloud_firestore.dart' as fire;
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' as prefix0;
import 'package:latlong/latlong.dart';
import 'package:http/http.dart' as http;
import 'package:geojson/geojson.dart';
import 'package:geopoint/geopoint.dart';


Future<Ruta> procesarRuta(body) async {
  final geo = GeoJson();
  await geo.parse(body);
  Ruta ruta;
  GeoJsonLine linea = geo.features.first.geometry;

  List<GeoPoint> geoPoints = linea.geoSerie.geoPoints;
  ruta = Ruta(coordenadas: geoPoints);

  ruta.puntosPersistidos = geoPoints.map((value) =>
  Lugar()
    ..longitud = value.longitude
    ..latitud = value.latitude).toList();
  Map<String, dynamic> json = geo.features.first.properties;


    List <dynamic> segmentos = json["segments"];
    List<Instruccion> instrucciones = [];
    segmentos.first["steps"].forEach((step) {
      instrucciones.add(Instruccion.fromMap(step));
    });

    double distancia = segmentos.first["distance"];
    double duration = segmentos.first["duration"];

    ruta.setDistancia(distancia);
    ruta.setDuracion(duration);
    ruta.setCalorias();

    ruta.instrucciones = instrucciones;
    print("jsjs");
    return ruta;

}


Future<List<prefix0.Marker>> procesarMarkers(args) async {


  List<prefix0.Marker> lista = [];

  var snap = args[0];



  for(int i = 0; i < snap.documents.length && i < 100; i++)
    {

    fire.GeoPoint geoP = snap.documents[i].data["ubicacion"];
    String tipo = snap.documents[i].data["tipo"];

    prefix0.BitmapDescriptor icono;


    switch(tipo){
      case "HUECO":
        icono = args[1];
        break;

      case "BAJARSE":
        icono = args[2];
      break;

      case "PARQUEO":
        icono = args[3];
        break;
    }

    prefix0.LatLng ubicacion = prefix0.LatLng(geoP.latitude, geoP.longitude);
    lista.add(prefix0.Marker(markerId: prefix0.MarkerId(ubicacion.toString()), position: ubicacion , icon: icono));
    }


  return lista;
}


class RutaRepo
{

  final fire.Firestore _db = fire.Firestore.instance;
  fire.CollectionReference ref;

  final String apiRuta ="https://api.openrouteservice.org/v2/directions/";
  final String apiKey =   "api_key=5b3ce3597851110001cf6248d80395b0b4774017bab91a3fa167dada";

  final String apiPreferencias="";

  //TODO: Agregar la instancia de Firebase




  Future<Ruta> calcularRuta (@required Coordinates puntoInicial, @required Coordinates puntoFinal,bool cicloRuta) async
  {

      String bicicleta =  cicloRuta? "cycling-regular?" : "cycling-road?";

      String url = apiRuta + bicicleta + apiKey + "&start=${puntoInicial.longitude.toString()},${puntoInicial.latitude.toString()}"
          "&end=${puntoFinal.longitude.toString()},${puntoFinal.latitude.toString()}";
      print("url ${url}");

      final response = await http.get(url).timeout(Duration(seconds: 6)).catchError((error){throw Exception("Ocurrió un error en la conexión");});;

      Ruta ruta;
      if(response.statusCode == 200)
        { 

          try{
            ruta = await compute(procesarRuta, response.body);
          }catch(error)
    {
      print(error);
    }

        }else{
        print("ENTRA AL NO 200");
        throw HttpException("Error en la conección",);
      }

      ruta.ciclorruta = cicloRuta;
    return ruta;
  }




  Future<Ruta> calcularRutaConParada(@required Coordinates puntoInicial, @required Coordinates puntoFinal,
  @required Coordinates puntoParada, bool cicloRuta) async
  {
    String bicicleta =  cicloRuta? "cycling-regular?" : "cycling-road?";

    String url1 = apiRuta + bicicleta + apiKey + "&start=${puntoInicial.longitude.toString()},${puntoInicial.latitude.toString()}"
        "&end=${puntoParada.longitude.toString()},${puntoParada.latitude.toString()}";
    print("url ${url1}");

    String url2 = apiRuta + bicicleta + apiKey + "&start=${puntoParada.longitude.toString()},${puntoParada.latitude.toString()}"
        "&end=${puntoFinal.longitude.toString()},${puntoFinal.latitude.toString()}";

    print("url ${url1}");


    final response = await http.get(url1).timeout(Duration(seconds: 6)).catchError((error){throw Exception("Ocurrió un error en la conexión");});
    final response2 = await http.get(url2).timeout(Duration(seconds: 6)).catchError((error){throw Exception("Ocurrió un error en la conexión");});

    Ruta ruta;
    List<GeoPoint> geoPoints1;

    double durationTotal;
    double distanciaTotal;

    List<Instruccion> instrucciones = [];

    if(response.statusCode == 200)
    {

      final geo = GeoJson();
      await geo.parse(response.body).catchError((error){throw Exception("Error en la conexión");});

      GeoJsonLine linea = geo.features.first.geometry;
      geoPoints1 =  linea.geoSerie.geoPoints;

      Map<String, dynamic> json = geo.features.first.properties;
      List <dynamic> segmentos = json["segments"];


      segmentos.first["steps"].forEach((step){instrucciones.add(Instruccion.fromMap(step));});

      distanciaTotal= segmentos.first["distance"];
      print("disntance $distanciaTotal");
      durationTotal= segmentos.first["duration"];

    }else{
      throw HttpException("Error en la conexión",);
    }

    List<GeoPoint> geoPoints2;
    if(response2.statusCode == 200)
      {

        final geo = GeoJson();
        await geo.parse(response2.body).catchError((error){throw Exception("Error en la conexión");});

        GeoJsonLine linea = geo.features.first.geometry;
        geoPoints2 =  linea.geoSerie.geoPoints;

        Map<String, dynamic> json = geo.features.first.properties;
        List <dynamic> segmentos = json["segments"];

        segmentos.first["steps"].forEach((step){instrucciones.add(Instruccion.fromMap(step));});



        distanciaTotal+= segmentos.first["distance"];
        print("disntance $distanciaTotal");
        durationTotal+= segmentos.first["duration"];

      }else{
      throw HttpException("Error en la conexión",);
    }

    geoPoints1.addAll(geoPoints2);

    ruta =  Ruta(coordenadas: geoPoints1);
    ruta.puntosPersistidos = geoPoints1.map((value)=> Lugar()..longitud = value.longitude..latitud=value.latitude).toList();
    ruta.setDistancia(distanciaTotal);
    ruta.setDuracion(durationTotal);
    ruta.setCalorias();
    ruta.instrucciones = instrucciones;

    ruta.ciclorruta = cicloRuta;


    return ruta;

  }

  void postMarcadores(String tipo, fire.GeoPoint ubicacion){
    try{
      String path = "markersMapa";
      ref = _db.collection(path);

      ref.add({"tipo":tipo, "ubicacion": ubicacion});
    }catch(error)
    {
      print("ERROR EN REPO" + error.toString());
    }
  }

  Future<List<prefix0.Marker>> getMarcadores() async
  {

    String path = "markersMapa";
    ref = _db.collection(path);
    fire.QuerySnapshot snap = await ref.getDocuments();

    List<dynamic> args = [];

    args.add(snap);

    prefix0.BitmapDescriptor iconoParqueo = await prefix0.BitmapDescriptor.fromAssetImage(ImageConfiguration(),"images/parking.png");
    prefix0.BitmapDescriptor iconoBajarse = await prefix0.BitmapDescriptor.fromAssetImage(ImageConfiguration(),"images/caminar.png");
    prefix0.BitmapDescriptor iconoHueco = await prefix0.BitmapDescriptor.fromAssetImage(ImageConfiguration(),"images/hueco.png");

    args.add(iconoHueco);
    args.add(iconoBajarse);
    args.add(iconoParqueo);



    List<prefix0.Marker> lista = await compute(procesarMarkers, args).catchError((e){print(e);});
    return lista;

  }


  void postRuta(Ruta ruta, String uid){
    try{
      String path = "rutas/$uid/rutas";
      ref = _db.collection(path);
      ruta.timeStamp = fire.Timestamp.now();
      ref.add(ruta.toJson());
    }catch(error)
    {
      print("ERROR EN REPO" + error.toString());
    }
}

Future<List<Ruta>> getRutasUser(String uid) async{
    print(uid);
  String path = "rutas/$uid/rutas";
  ref = _db.collection(path);
  fire.QuerySnapshot snap = await ref.getDocuments();
  return snap.documents
      .map((doc) => Ruta.fromMap(doc.data))
      .toList();
}



Future<List<Map<String,dynamic>>> getSugerencias(String texto) async{

    String url = "https://api.openrouteservice.org/geocode/autocomplete?$apiKey&text=$texto&focus.point.lon=-74.0817500&focus.point.lat=4.6097100&boundary.country=CO&layers=address,venue";
    print(url);
    final response = await http.get(url);

    if(response.statusCode == 200)
      {
        final geo = GeoJson();
        await geo.parse(response.body);

       List<dynamic> lista =  geo.features;

       List<Map<String, dynamic>> retorno = [];

       lista.forEach((f){

         final Map<String, dynamic> mapa = f.properties;

         GeoJsonPoint geometry = f.geometry;
         mapa['geometry'] = geometry.geoPoint;
         retorno.add(mapa);
       });

       return retorno;

      }

}






}