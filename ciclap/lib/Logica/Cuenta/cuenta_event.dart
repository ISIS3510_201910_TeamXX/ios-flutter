import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CuentaEvent extends Equatable {
  CuentaEvent([List props = const[]]): super(props);
}


class CargarCuenta extends CuentaEvent{

  final String uId;

  CargarCuenta(this.uId):super([uId]);

}

class EditarNombre extends CuentaEvent{



}


