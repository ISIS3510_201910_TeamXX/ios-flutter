import 'package:ciclap/VOs/InstruccionVO.dart';
import 'package:ciclap/VOs/LugarVO.dart';
import 'package:cloud_firestore/cloud_firestore.dart' as prefix0;
import 'package:geojson/geojson.dart';
import 'package:geopoint/geopoint.dart';
import 'package:hive/hive.dart';
import 'package:latlong/latlong.dart';


part 'RutaVO.g.dart';

@HiveType()
class Ruta
{
  @HiveField(0)
  String nombre; //Puede ser null
  @HiveField(1)
  Lugar puntoSalida;
  @HiveField(2)
  Lugar puntoFinal;
  @HiveField(3)
  Lugar puntoParada;
  @HiveField(4)
  bool conParada;
  @HiveField(5)
  List<Lugar> puntosPersistidos;
  @HiveField(6)
  List<Instruccion> instrucciones;
  @HiveField(7)
  String distancia;
  @HiveField(8)
  String duracion;
  @HiveField(9)
  String calorias;

  bool ciclorruta;


  prefix0.Timestamp timeStamp;
  double latitudInicio;
  double longitudInicio;

  double latitudFinal;
  double longitudFinal;

  double longitudParada;
  double latitudParada;


  List<GeoPoint> coordenadas;
  @HiveField(10)
  double _distanciaDouble;
  @HiveField(11)
  double _duracionDouble;
  @HiveField(12)
  double _caloriasDouble;

  String parametroAltura;

  Ruta({this.nombre, this.puntoSalida,this.puntoParada,this.puntoFinal, this.coordenadas, this.instrucciones});

  Ruta.fromMap(Map snapshot) :

        _caloriasDouble = snapshot['calorias'].toDouble() ?? 0,
        _duracionDouble = snapshot['duracion'].toDouble() ?? 0,
        _distanciaDouble = snapshot['distancia'].toDouble() ?? 0,
        timeStamp = snapshot['timeStamp'] ?? '',
        puntoSalida =  Lugar(nombre:snapshot['nombreInicio']),
        puntoFinal = Lugar(nombre: snapshot['nombreFinal']),
        puntoParada = Lugar(nombre: snapshot['nombreParada']),
        conParada = snapshot['conParada']??false,
        latitudInicio =(snapshot['latitudInicio'] is String)? double.parse(snapshot['latitudInicio']):snapshot['latitudInicio'],
        longitudInicio = (snapshot['longitudInicio'] is String)? double.parse(snapshot['longitudInicio']):snapshot['longitudInicio'],
        latitudFinal = (snapshot['latitudFinal'] is String)?double.parse(snapshot['latitudFinal']):snapshot['latitudFinal'] ,
        longitudFinal = (snapshot['longitudFinal'] is String)?double.parse(snapshot['longitudFinal']):snapshot['longitudFinal'],
        latitudParada = (snapshot['latitudParada'] is String)?double.parse(snapshot['latitudParada']):snapshot['latitudParada'] ,
        longitudParada = (snapshot['longitudParada'] is String)?double.parse(snapshot['longitudParada']):snapshot['longitudParada'],
        ciclorruta = snapshot['ciclorruta']??false;
  toJson ()
  {
    return{
      "nombreInicio": puntoSalida.nombre,
      "nombreFinal": puntoFinal.nombre,
      "conParada": conParada,
      "nombreParada": puntoParada != null ? puntoParada.nombre: "",
      "duracion": _duracionDouble,
      "distancia": _distanciaDouble,
      "calorias": _caloriasDouble,
      "timeStamp": timeStamp,
      "latitudInicio": latitudInicio ?? 0,
      "longitudInicio": longitudInicio ?? 0,
      "latitudFinal": latitudFinal?? 0,
      "longitudFinal": longitudFinal ?? 0,
      "latitudParada": latitudParada ?? 0,
      "longitudParada": longitudParada ?? 0,
      "ciclorruta": ciclorruta ?? false
    };
  }

  void setDistancia(double pDistancia){
      _distanciaDouble = pDistancia;
      distancia = (_distanciaDouble/1000).toStringAsFixed(2) + "km";
  }

//Recibe duracion en segundos, setea en string con el formato
  void setDuracion(double pDuracion){
    _duracionDouble = pDuracion;

    int min = Duration(seconds: pDuracion.toInt()).inMinutes;
    int horas = (min/60).floor();
    int minutos = min%60;

    duracion = "${horas==0?"": horas.toString()+"h"}${minutos}min";
  }

  //Calculado a partir de P = g * m * Vg * (K1 + s) + K2 * Va2 * Vg con valores promedios
  void setCalorias(){
      _caloriasDouble = _duracionDouble*0.07;
       calorias= "${(_duracionDouble*0.07).toStringAsFixed(2)}kcal";
  }


  static String duracionToString(double duracion){
    int min = Duration(seconds: duracion.toInt()).inMinutes;
    int horas = (min/60).floor();
    int minutos = min%60;
    return "${horas==0?"": horas.toString()+"h"}${minutos}min";
  }

  static distanciaToString(double distancia){
    return  (distancia/1000).toStringAsFixed(2) + "km";
  }

  static caloriasToString(double calorias){
      return "${calorias.toStringAsFixed(2)}kcal";
  }


  String getFormatoAltura()
  {

    String retorno = "";

    for(int i = 0; i < coordenadas.length; i++)
      {
        var coor = coordenadas[i];
        retorno += coor.latitude.toString() + "," + coor.longitude.toString() + (i!=coordenadas.length-1? ",": "");
      }


    return retorno;
  }


}



