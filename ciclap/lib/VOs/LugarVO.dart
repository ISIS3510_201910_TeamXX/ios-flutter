
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';

part 'LugarVO.g.dart';

@HiveType()
class Lugar{

  @HiveField(0)
  String nombre;
  @HiveField(1)
  String nombreCompleto;

  @HiveField(2)
  double latitud;
  @HiveField(3)
  double longitud;

  String descripcion;

  LatLng ubicacion;

  Lugar({this.descripcion, this.nombre, this.ubicacion});

}