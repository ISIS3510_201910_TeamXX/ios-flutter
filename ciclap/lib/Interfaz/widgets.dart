import 'package:ciclap/Interfaz/VisualizarTerreno.dart';
import 'package:ciclap/Repositorio/RutaRepo.dart';
import 'package:ciclap/VOs/LugarVO.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:ciclap/Logica/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:geopoint/geopoint.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';

const Color negro = Color(0xFF353531);
const Color amarillo = Color(0xFFFBBC05);
const Color morado = Color(0xFF5F0F40);
const Color morado2 = Color(0xFF814369);
const Color grisClaro = Color(0xfff0f0f0);
const Color grisSombra = Color(0xffe9e9e9);

//--------------------------Rutas------------------------------
class Rutas extends StatefulWidget {
  Ruta ruta;

  Rutas(this.ruta);

  @override
  _RutasState createState() => _RutasState();
}

class _RutasState extends State<Rutas> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: FlatButton(
        padding: EdgeInsets.all(0),
        color: Colors.white,
        onPressed: (){
          List<GeoPoint> geoPoints = widget.ruta.puntosPersistidos.map((value)=>GeoPoint(latitude: value.latitud, longitude: value.longitud)).toList();
          widget.ruta.coordenadas = geoPoints;
          Navigator.push(context, MaterialPageRoute(builder: (context)=> VisualizarTerreno(conParada: false, ruta:widget.ruta ,)));
        },
        child: Container(

          width: MediaQuery.of(context).size.width * 0.94,
          height: MediaQuery.of(context).size.height * 0.11,
          child: Row(
            children: <Widget>[
              Container(
                  width: MediaQuery.of(context).size.width * 0.69,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //TEXTO ARRIBA
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 13),
                        child: Text(
                          "Ruta", //TODO: Cambiar texto Nombre
                          style: TextStyle(
                              fontFamily: "Roboto",
                              fontSize: 24,
                              color: Colors.black),
                        ),
                      ),

                      //TEXTO ABAJO
                      Container(
                        width: MediaQuery.of(context).size.width*20,
                        height: 30,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                              child: Text(
                                widget.ruta.puntoSalida.nombre, //TODO: Cambiar direccion origen
                                style: TextStyle(
                                  fontSize: 11,
                                ),
                              ),
                            ),
                            Icon(
                              Icons.arrow_forward,
                              color: Colors.grey,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 8),
                              child: Text(
                                widget.ruta.puntoFinal.nombre, //TODO: Cambiar Dirección destino
                                style: TextStyle(fontSize: 11, color: Colors.grey),
                              ),
                            )
                          ],
                        ),
                      ),

                    ],
                  )
              ),
              Padding(
                padding: const EdgeInsets.all(9.0),
                child: VerticalDivider(
                  indent: 4,
                  endIndent: 4,
                ),
              ),
              Image.asset(
                "images/bicicleta.png",
                width: 40,
              )
            ],
          ),
        ),
      ),
    );
  }
}
//--------------------------END-Rutas------------------------------

//--------------------------TextoIcono----------------------------------
class TextoIcono extends StatefulWidget {
  String asset;
  Text textoOtroStyle;
  String texto = "";
  Icon icono;

  TextoIcono({this.asset, this.texto, this.textoOtroStyle, this.icono});

  @override
  _TextoIconoState createState() => _TextoIconoState();
}

class _TextoIconoState extends State<TextoIcono> {
  Widget _buildChildIcono() {
    if (widget.asset == null) {
      return widget.icono;
    }
    return Image.asset(
      widget.asset,
      width: 30,
      height: 30,
    );
  }

  Widget buildChildTexto() {
    if (widget.texto != null) {
      return Text(
        widget.texto,
        style:
            TextStyle(fontSize: 25, color: negro, fontWeight: FontWeight.w300),
      );
    }
    return widget.textoOtroStyle;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(padding: const EdgeInsets.all(8.0), child: _buildChildIcono()),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: buildChildTexto(),
        )
      ],
    );
  }
}
//--------------------------END-TextoIcono------------------------------

//--------------------------BotonSecundario------------------------------
class BotonSecundario extends StatelessWidget {
  Widget hijo;
  final GestureTapCallback onPressed;

  BotonSecundario({@required this.hijo, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return OutlineButton(
      splashColor: Colors.transparent,
      child: hijo,
      borderSide: BorderSide(color: grisSombra),
      highlightedBorderColor: morado,
      onPressed: onPressed,
    );
  }
}
//--------------------------END-BotonSecundario------------------------------

class BotonPrimario extends StatelessWidget {
  String texto = "";
  Color color;
  GestureTapCallback onPressed;

  BotonPrimario({this.texto, this.color, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      padding: EdgeInsets.all(5),
      color: color,
      disabledColor: Colors.grey,
      child: Text(
        texto,
        style: TextStyle(
            color: Colors.white, fontSize: 22, fontWeight: FontWeight.w300),
      ),
      onPressed: onPressed,
    );
  }
}

//--------------------------SearchBar------------------------------
class SearchBar extends StatefulWidget {
  String textoHint;
  bool parada;
  bool puntoInicio = false;
  bool disabled = false;
  TextEditingController controller;

  SearchBar({this.textoHint, this.parada, this.controller, this.puntoInicio, this.disabled});

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 6.0),
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(2), color: grisClaro),
          width: MediaQuery.of(context).size.width * 0.90,
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(12),
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color:
                        !widget.parada ? morado2.withOpacity(0.6) : Colors.grey,
                  ),
                  width: 10,
                  height: 10,
                ),
              ),
              Expanded(
                  child: TypeAheadField(
                    suggestionsCallback: (pattern) async {
                      return await RutaRepo().getSugerencias(pattern);
                    },
                    itemBuilder: (context, suggestion) {
                      bool houseNull = suggestion['housenumber'] == null;
                      String street =  suggestion['street'] ?? '';
                      String numeral = houseNull? '' : '#';
                      String houseNumber = houseNull? '' : suggestion['housenumber'];

                      String detalles = street + numeral + houseNumber;

                      return ListTile(
                        leading: Icon(Icons.location_on),
                        title: Text(suggestion['name']),
                        subtitle: Text(suggestion['county'] + ", " + detalles),
                      );
                    },
                    onSuggestionSelected: (suggestion) {
                      Lugar lugar = Lugar();
                      lugar.nombre = suggestion['name'];
                      lugar.ubicacion = LatLng(suggestion['geometry'].latitude, suggestion['geometry'].longitude);

                      String tipoLugar;

                      widget.controller.text = lugar.nombre; 

                      if(widget.puntoInicio)
                        {
                          tipoLugar = "inicio";
                        }else if(widget.parada)
                          {
                            tipoLugar = "parada";
                          }else{
                          tipoLugar = "destino";
                      }

                      BlocProvider.of<DestinoBloc>(context).dispatch(SeleccionarDeSuggestion(tipoLugar, lugar));

                    },
                    /*onTap: () async {
                      // show input autocomplete with selected mode
                      // then get the Prediction selected
                      Prediction p = await PlacesAutocomplete.show(
                          language: "es",
                          onError: (res) {
                            PlacesAutocompleteResponse sls = res;
                            print(sls.errorMessage);
                          },
                          components: [new Component(Component.country, "co")],
                          mode: Mode.overlay,
                          context: context, apiKey: "AIzaSyAzGtpaQedWGpXtfwyYBaCq4dcr0yUtKAA");
                    },*/
                    textFieldConfiguration: TextFieldConfiguration(

                        decoration: InputDecoration(
                          hintText: widget.textoHint,
                        ),
                      controller: widget.controller,
                    ),

              )),
              widget.puntoInicio
                  ? BlocBuilder(
                      bloc: BlocProvider.of<DestinoBloc>(context),
                      builder: (BuildContext context, DestinoState estado) {
                        return IconButton(
                            icon: Icon(Icons.my_location),
                            onPressed: (widget.disabled)
                                ? null
                                : () {

                                    BlocProvider.of<DestinoBloc>(context).dispatch(SeleccionarMiUbicacion());
                                  });
                      },
                    )
                  : Container(),
            ],
          )),
    );
  }
}

//--------------------------END-SearchBar------------------------------
class SearchedBar extends StatefulWidget {
  String texto = "";
  double width;


  SearchedBar(this.texto,[this.width]);

  @override
  _SearchedBarState createState() => _SearchedBarState();
}

class _SearchedBarState extends State<SearchedBar> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3.0),
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(2),
              border: Border.all(color: Colors.grey)),
          width: widget.width ?? MediaQuery.of(context).size.width * 0.90,
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10),
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: amarillo.withOpacity(0.6),
                  ),
                  width: 10,
                  height: 10,
                ),
              ),
              Expanded(child: Text(widget.texto))
            ],
          )),
    );
  }
}

//--------------------------END-SearchBar------------------------------

//--------------------------TextoPlus---------------------------------
class TextoPlus extends StatelessWidget {
  String texto;
  bool quitar;
  Icon plus = Icon(Icons.add, color: morado);
  Icon menos = Icon(CupertinoIcons.clear_thick, color: morado);

  TextoPlus({this.texto, this.quitar});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      padding: EdgeInsets.zero,
      child: Row(
        children: <Widget>[
          quitar ? menos : plus,
          Text(
            texto,
            style: TextStyle(fontSize: 18, color: morado),
          )
        ],
      ),
      onPressed: null,
    );
  }
}
//--------------------------END-TextoPlus---------------------------------

class MensajeError extends StatelessWidget {
  Widget pantalla;
  String error;
  VoidCallback callback;
  bool detalles;

  MensajeError(this.pantalla, this.error, this.callback);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        pantalla
        ,Opacity(opacity: 0.5,
          child: ModalBarrier(
            color: Colors.grey,dismissible: false,
          ),
        ),
        Center(child: Container(
          width: MediaQuery.of(context).size.width * 0.8,
          height: MediaQuery.of(context).size.height*0.45,
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height*0.06,
                color: Colors.white,
                child: Stack(
                  children: <Widget>[
                    Align(alignment: Alignment.center, child: Text("Oops!", style: TextStyle(fontSize: 22),),),
                    Positioned(top: 6, left: 10,child: Icon(Icons.error_outline, color: Color(0xFFEC6E6E),size: 30,)),
                  ],
                ),
              ),Container( //DIVIDER
                width: MediaQuery.of(context).size.width * 0.8,
                height: 1,
                color: Colors.grey,

              ),
              
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Text(error, style: TextStyle(fontSize: 18),),
              ),

              Expanded(child: Container()),

              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(

                  children: <Widget>[

                    FlatButton(onPressed:callback, child: Text("OK", style: TextStyle(color: Colors.blueAccent, fontSize: 18),))
                  ],
                ),
              )
            ],

          ),


        ),)

      ],
    );
  }
}









class CuadroPerfil extends StatefulWidget {
  String textoGrande;
  String evitado = "";
  String categoria;
  String asset;
  String textoSecundario;

  CuadroPerfil(
      {this.categoria,
      this.asset,
      this.evitado,
      this.textoGrande,
      this.textoSecundario});

  @override
  _CuadroPerfilState createState() => _CuadroPerfilState();
}

class _CuadroPerfilState extends State<CuadroPerfil> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.48,
      height: MediaQuery.of(context).size.width * 0.44,
      decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.all(5), child: Text(widget.evitado))
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Text(
                  widget.textoGrande,
                  style: TextStyle(fontSize: 25),
                ),
              ),
              Text(
                widget.textoSecundario,
                style: TextStyle(fontSize: 16, color: Colors.grey),
              )
            ],
          ),
          Flexible(
              child: Container(
            decoration: BoxDecoration(color: grisSombra),
            child: TextoIcono(
              textoOtroStyle: Text(
                widget.categoria,
                style: TextStyle(fontSize: 20),
              ),
              asset: widget.asset,
            ),
          ))
        ],
      ),
    );
  }
}

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final List<Icon> icons;
  const AppBarWidget(this.icons, this.title);
  @override
  Size get preferredSize => new Size.fromHeight(70);

  @override
  Widget build(BuildContext context) {
    double size = icons.length.toDouble();

    return new AppBar(
      backgroundColor: Colors.white,
      bottom: PreferredSize(
          child: Container(
            color: amarillo,
            height: 2.0,
          ),
          preferredSize: Size.fromHeight(0)),
      title: Container(
        width: MediaQuery.of(context).size.width,
        height: 100,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Row(
              children: icons,
            ),
            Padding(
              padding: EdgeInsets.only(left: 100 / (size * 5)),
              child: Text(
                title,
                style: TextStyle(fontSize: 26),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class HistoryCard extends StatefulWidget {
  final String from;
  final String to;
  final String date;

  HistoryCard(
      {Key key, @required this.date, @required this.from, @required this.to})
      : super(key: key);

  _HistoryCardState createState() => _HistoryCardState();
}

class _HistoryCardState extends State<HistoryCard> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Container(
          height: 120,
          margin: EdgeInsets.symmetric(vertical: 8.0),
          width: MediaQuery.of(context).size.width * 0.90,
          child: Card(
            shape: Border.all(color: negro, width: 0.5),
            child: Padding(
              padding: const EdgeInsets.only(left: 18.0),
              child: Row(
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.arrow_downward,
                            size: 16,
                          ),
                          Text(
                            widget.from,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.arrow_upward,
                            size: 16,
                          ),
                          Text(
                            widget.to,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            widget.date,
                            style: TextStyle(
                                color: grisSombra, fontWeight: FontWeight.bold),
                          )
                        ],
                      )
                    ],
                  ),
                 Expanded(child: Container(),),
                  Container(
                    height: 80,
                    margin: EdgeInsets.only(right: 0),
                    child: VerticalDivider(width: 2, color: grisClaro),
                  ),
                  Container(width: MediaQuery.of(context).size.width *0.04,),
                  Image.asset(
                    "images/bicicleta.png",
                    alignment: Alignment.topRight,
                  ),
                  Container(width: MediaQuery.of(context).size.width *0.04,),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class HelpItem extends StatelessWidget {
  final IconData icon;
  final String mainText;
  final String secondarytext;
  const HelpItem(this.icon, this.mainText, this.secondarytext);
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 8.0),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: amarillo),
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Icon(
              icon,
              color: Colors.grey,
              size: 60,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20.0),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.65,
            child: Column(
              children: <Widget>[
                Text(
                  mainText,
                  style: TextStyle(
                      color: Colors.grey,
                      fontWeight: FontWeight.w900,
                      height: 1.5),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8.0),
                ),
                Text(secondarytext,
                    style: TextStyle(
                        color: Colors.grey, fontWeight: FontWeight.w400)),
              ],
            ),
          )
        ],
      ),
      Padding(
        padding: EdgeInsets.only(top: 8.0),
      ),
      Divider(
        color: Colors.grey[300],
        indent: 30,
        endIndent: 30,
      )
    ]);
  }
}

