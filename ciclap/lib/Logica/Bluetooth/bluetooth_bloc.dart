import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter_blue/flutter_blue.dart';
import '../bloc.dart';

class BluetoothBloc extends Bloc<BluetoothEvent, BluetoothEstado> {
  @override
  BluetoothEstado get initialState => InitialBluetoothState();
  FlutterBlue flutterBlue = FlutterBlue.instance;

  @override
  Stream<BluetoothEstado> mapEventToState(
    BluetoothEvent event,
  ) async* {


    if(event is Refresh)
      {
        print("sks");
        bool prendido = await flutterBlue.isOn;

        if(prendido)
          {

            yield(Cargando());

            flutterBlue.startScan(timeout: Duration(seconds: 10));
            Map<dynamic, String> mapa= Map();
            var subscription = flutterBlue.scanResults.listen((scanResult) {

              if(scanResult.isNotEmpty)
                {
                  for(var i in scanResult)
                    {
                      mapa.putIfAbsent(i.device.id.toString(), ()=>"sdf");
                    }

                }


            });

            await Future.delayed(Duration(seconds: 10));
            print(mapa);
            subscription.cancel();

            flutterBlue.stopScan();
            currentState.buscando = false;

            yield(CargadoBlue());
          }else{

          yield(PrendaBlueTooth());
        }
        // Start scanning


      }
  }
}
