import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BluetoothEstado extends Equatable{

  bool buscando;


}

class InitialBluetoothState extends BluetoothEstado {


}

class PrendaBlueTooth extends BluetoothEstado{

}

class Cargando extends BluetoothEstado{

}


class CargadoBlue extends BluetoothEstado{

}