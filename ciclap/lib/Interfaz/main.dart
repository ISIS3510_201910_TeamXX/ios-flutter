
import 'package:ciclap/Interfaz/Home.dart';
import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/Repositorio/AutenticacionRepo.dart';
import 'package:ciclap/Repositorio/RutaRepo.dart';
import 'package:ciclap/VOs/InstruccionVO.dart';
import 'package:ciclap/VOs/LugarVO.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ciclap/Interfaz/widgets.dart';
import 'package:ciclap/Interfaz/PonerDestino.dart';
import 'package:ciclap/Interfaz/Cuenta.dart';
import 'package:ciclap/Interfaz/PuntosInteres.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_offline/flutter_offline.dart';

import 'package:ciclap/Interfaz/Hisotrial.dart';

import 'package:ciclap/Interfaz/Ayuda.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path;

const Color negro = Color(0xFF353531);
const Color amarillo = Color(0xFFFBBC05);
const Color morado = Color(0xFF5F0F40);

void main() async {

  final AutenticacionRepo usuarioRepo = AutenticacionRepo();
  MyApp.pathHive = await path.getApplicationDocumentsDirectory();
  Hive.init(MyApp.pathHive.path);

  Hive.registerAdapter(RutaAdapter(), 0);
  Hive.registerAdapter(LugarAdapter(), 1);
  Hive.registerAdapter(InstruccionAdapter(), 2);

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(BlocProvider(
        builder: (context) => AutenticacionBloc(userRepository:usuarioRepo)..dispatch(AppStarted()),
        child: MyApp(usuarioRepo)
    )
    );
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  static var pathHive;
  AutenticacionRepo usuarioRepo;
  MyApp(this.usuarioRepo):assert(usuarioRepo!=null);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        backgroundColor: Colors.white,
        primaryColor: Color(0xFFFBBC05),
      ),
      home: MyHomePage(usuarioRepo: usuarioRepo,)
      );
  }
}

class MyHomePage extends StatefulWidget {

  MyHomePage({Key key, this.usuarioRepo} ) :assert (usuarioRepo!=null), super(key: key);
  AutenticacionRepo usuarioRepo;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  void abrirCaja(){
    final boxRutas = Hive.openBox("rutas");
  }

  @override
  Widget build(BuildContext context) {

    abrirCaja();
    return BlocBuilder<AutenticacionBloc, AutenticacionState>(
        builder: (context, state) {
      if (state is Uninitialized) {
      return Cargando();
      }else if(state is Authenticated)
      {
       return Home();
    }
    return LoginScreen(userRepository: widget.usuarioRepo,);
    },
    );
  }
}


class Cargando extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Container(width: MediaQuery.of(context).size.width,height: MediaQuery.of(context).size.height,color:Colors.white),
      Align(child:CupertinoActivityIndicator(radius: 20,), alignment: Alignment.center)
    ],);
  }
}


class LoginScreen extends StatelessWidget {
  final AutenticacionRepo _userRepository;

  LoginScreen({Key key, @required AutenticacionRepo userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<LoginBloc>(
        builder: (context) => LoginBloc(userRepository: _userRepository),
        child: LoginForm(userRepository: _userRepository),
      ),
    );
  }
}


class LoginForm extends StatefulWidget {
  final AutenticacionRepo _userRepository;

  LoginForm({Key key, @required AutenticacionRepo userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  LoginBloc _loginBloc;

  AutenticacionRepo get _userRepository => widget._userRepository;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state.isFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Error en el inicio de sesión'),
                    Icon(Icons.error)
                  ],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
        if (state.isSubmitting) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Iniciando Sesión ...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          BlocProvider.of<AutenticacionBloc>(context).dispatch(LoggedIn());
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20.0),
            child: Form(
              child: ListView(

                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 60),
                    child: Image.asset('images/logoGrande.png', height: 100),
                  ),
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.email),
                      labelText: 'Email',
                    ),
                    autovalidate: true,
                    autocorrect: false,
                    validator: (_) {
                      return !state.isEmailValid ? 'Invalid Email' : null;
                    },
                  ),
                  TextFormField(
                    controller: _passwordController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.lock),
                      labelText: 'Contraseña',
                    ),
                    obscureText: true,
                    autovalidate: true,
                    autocorrect: false,
                    validator: (_) {
                      return !state.isPasswordValid ? 'Invalid Password' : null;
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        SizedBox(height: MediaQuery
                            .of(context)
                            .size
                            .height * 0.2,),
                        BotonPrimario(
                          onPressed: () {
                            if (_emailController.text == null ||
                                _emailController.text.isEmpty ||
                                _passwordController.text.isEmpty ||
                                _passwordController.text == null) {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text("Complete todos los campos")));
                            } else {
                              _onFormSubmitted();
                            }
                          },
                          color: amarillo,
                          texto: "Iniciar Sesion",
                        ),
                        SizedBox(height: 5,),
                        BotonGoogle(),
                        FlatButton(onPressed: () => Navigator.push(context,
                            MaterialPageRoute(builder: (context) => RegisterScreen(userRepository: _userRepository))),
                            child: Text("¿No tienes cuenta?", style: TextStyle(
                                color: Colors.blueAccent, fontSize: 16),)
                        )
                        //CreateAccountButton(userRepository: _userRepository),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    Hive.box("rutas").close();
    super.dispose();
  }

  void _onFormSubmitted() {
    _loginBloc.dispatch(
      LoginWithCredentialsPressed(
        email: _emailController.text,
        password: _passwordController.text,
      ),
    );
  }

  Widget BotonGoogle() {
    return Container();
    /*  return FlatButton(onPressed: null,
        padding: EdgeInsets.all(10),
        child: Container(
          decoration:BoxDecoration(border: Border.all(color: Colors.redAccent)) ,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextoIcono(icono: Icon(FontAwesomeIcons.google,color: Colors.grey,),textoOtroStyle: Text("Iniciar sesion con Google", style: TextStyle(fontSize: 17, color: Colors.black),),),
            ],
          ),
        ));
  */
  }
}


class RegisterScreen extends StatelessWidget {
  final AutenticacionRepo _userRepository;

  RegisterScreen({Key key, @required AutenticacionRepo userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget([Icon(Icons.contact_mail)],"Registro"),
      body: Center(
        child: BlocProvider<RegisterBloc>(
          builder: (context) => RegisterBloc(userRepository: _userRepository),
          child: RegisterForm(),
        ),
      ),
    );
  }
}

class RegisterForm extends StatefulWidget {
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _nombreController = TextEditingController();
  final TextEditingController _pesoController = TextEditingController();
  final TextEditingController _fechaController = TextEditingController();
  DateTime selectedDate = DateTime.now();

  RegisterBloc _registerBloc;

  bool fechaSel = false;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isRegisterButtonEnabled(RegisterState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);

  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state.isSubmitting) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Registering...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          BlocProvider.of<AutenticacionBloc>(context).dispatch(LoggedIn());
          Navigator.of(context).pop();
        }
        if (state.isFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(

              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(state.error),
                    Icon(Icons.error),
                  ],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: ListView(
                children: <Widget>[
                  SizedBox(height: MediaQuery.of(context).size.height*0.08,),

                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.email),
                      labelText: 'Email',
                    ),
                    autocorrect: false,
                    autovalidate: true,
                    validator: (_) {
                      return !state.isEmailValid ? 'Correo no válido' : null;
                    },
                  ),
                  TextFormField(
                    controller: _passwordController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.lock),
                      labelText: 'Contraseña',
                    ),
                    obscureText: true,
                    autocorrect: false,
                    autovalidate: true,
                    validator: (_) {
                      return !state.isPasswordValid ? 'Contraseña no válido' : null;
                    },
                  ),
                  TextFormField(
                    controller: _nombreController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.account_box),
                      labelText: 'Nombre',
                    ),
                    autocorrect: false,
                    autovalidate: true,

                  ),
                  TextFormField(
                    controller: _pesoController,
                      keyboardType: TextInputType.phone,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                    decoration: InputDecoration(
                      icon: Icon(Icons.fitness_center),
                      labelText: 'Peso(kg.)',
                    ),
                    autocorrect: false,
                    autovalidate: true,

                  ),



                  FlatButton(
                      padding: EdgeInsets.zero,
                      onPressed: () => elegirFecha(context),
                      child: TextField(controller: _fechaController, decoration: InputDecoration(hintText: "Fecha Nacimiento", icon: Icon(Icons.calendar_today),), enabled: false,)

                  ),


                  SizedBox(height: 60 ,),
                  BotonPrimario(
                    texto: "Enviar",
                    color: morado,
                    onPressed: isRegisterButtonEnabled(state) && _pesoController.text != ""
                    && _nombreController.text != "" && _fechaController.text != ""
                        ? _onFormSubmitted
                        : null,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _fechaController.dispose();
    _nombreController.dispose();
    _pesoController.dispose();
    super.dispose();
  }


  Future<Null> elegirFecha(BuildContext context) async {

    final DateTime picked = await showDatePicker(
      initialDatePickerMode: DatePickerMode.year,
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1930, 1),
        lastDate: DateTime.now());
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        fechaSel = true;
        _fechaController.text = selectedDate.toString().substring(0, 10);

      });
  }


/*  void _onEmailChanged() {
    _registerBloc.add(
      EmailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _registerBloc.dispatch(
      PasswordChangedRegistro(password: _passwordController.text),
    );
  }*/

  void _onFormSubmitted() {
    _registerBloc.dispatch(
      SubmittedRegistro(
        email: _emailController.text,
        password: _passwordController.text,
        nombre: _nombreController.text,
        fecha:  selectedDate.toString(),
        peso: int.parse(_pesoController.text)
      ),
    );
  }
}