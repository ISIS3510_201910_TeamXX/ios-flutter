import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class RegisterEvent extends Equatable {


  RegisterEvent([List props = const []]) : super(props);
}

class EmailChangedRegistro extends RegisterEvent {
  final String email;

  EmailChangedRegistro({@required this.email}): super([email]);



  @override
  String toString() => 'EmailChanged { email :$email }';
}

class PasswordChangedRegistro extends RegisterEvent {
  final String password;

  PasswordChangedRegistro({@required this.password}):super([password]);

  @override
  String toString() => 'PasswordChanged { password: $password }';
}

class SubmittedRegistro extends RegisterEvent {
  final String email;
  final String password;
  final int peso;
  final String nombre;
  final String fecha;

   SubmittedRegistro({
    @required this.email,
    @required this.password,
     this.peso,this.nombre, this.fecha
  }):super([email, password,peso,nombre,fecha]);



  @override
  String toString() {
    return 'Submitted { email: $email, password: $password }';
  }
}