import 'package:ciclap/VOs/RutaVO.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RutasState extends Equatable{
  RutasState([List props = const[]]): super(props);

}

class InitialRutasState extends RutasState {

}



class RutasLoaded extends RutasState{

  final List<Ruta> rutas;
  RutasLoaded(this.rutas): super([rutas]);

}


class ErrorRuta extends RutasState{
  final String error;

  ErrorRuta(this.error):super([error]);
}
