import 'package:ciclap/Interfaz/VisualizarTerreno.dart' as vt;

import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:flutter/material.dart';
import 'package:ciclap/Interfaz/Mapa.dart';
import 'package:ciclap/Interfaz/widgets.dart' as w;
import 'package:ciclap/Interfaz/AnadirSticker.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:math' as math;

import 'package:url_launcher/url_launcher.dart';


class Navegacion extends StatefulWidget {
  final String destino;
  final VerTerrenoBloc terrenoBloc;
  final Ruta ruta;
  @override
  _NavegacionState createState() => _NavegacionState();

  Navegacion({@required this.destino, @required this.terrenoBloc, @required this.ruta});
}

class _NavegacionState extends State<Navegacion> {

  final NavegarBloc navegarBloc = NavegarBloc();

  GoogleMapa mapa = GoogleMapa(true);
  List stickers = ["PARQUEO", "HUECO", "BAJARSE"];
  
  @override
  Widget build(BuildContext context) {


    return StreamBuilder<Object>(
      stream: null,
      builder: (context, snapshot) {
        return BlocProvider(
          builder: (context) => navegarBloc,
          child: Scaffold(
              body: Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Row(children: <Widget>[


                        IconButton(
                          onPressed: () {
                            //widget.terrenoBloc.dispatch(CargarRuta(nEstado.ruta));
                            Navigator.of(context).popUntil((route) => route.isFirst);
                          },
                          icon: Icon(Icons.close),
                        ),
                        // w.SearchedBar( widget.ruta.puntoFinal.nombre , MediaQuery.of(context).size.width * 0.8),
                      ]),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.7,
                      child: Stack(
                        //alignment: Alignment.topCenter,
                        children: <Widget>[
                          BlocProvider<VerTerrenoBloc>(
                            builder: (context)=> widget.terrenoBloc,
                            child: BlocBuilder(
                              bloc: widget.terrenoBloc,
                              builder: (BuildContext contexto, VerTerrenoState estado)
                              {
                                return Positioned(
                                  child: Container(
                                    height: MediaQuery.of(context).size.height * 0.7,
                                    decoration: BoxDecoration(border: Border.all(color: Colors.black)),
                                    child: mapa,
                                  ),
                                );
                              },

                            ),
                          ),
                          Positioned(
                            top: 50,
                            left: (MediaQuery.of(context).size.width * 0.65) / 7,
                            width: MediaQuery.of(context).size.width * 0.8,
                            height: 80,
                            child: BlocBuilder(
                              bloc: navegarBloc,
                              builder: (BuildContext context, NavegarState estado)
                              {
                                if(estado is InitialNavegarState)
                                {
                                  navegarBloc.dispatch(ComenzarNavegacion(widget.ruta, (BlocProvider.of<AutenticacionBloc>(context).currentState as Authenticated).uid));
                                }
                                String ins ="";
                                String name ="";
                                Widget w = null;
                                if(estado is InsCargada)
                                  {
                                    ins = estado.instruccion;
                                    name = estado.name;
                                    w = estado.widget;
                                  }
                                if(estado is AgregandoSticker)
                                  {
                                    ins = estado.instruccion;
                                    name = estado.name;
                                    w = estado.widget;
                                  }
                                return InkWell(
                                  onTap: (){navegarBloc.dispatch(SiguienteInstruccion());},
                                  child: Card(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 8.0, vertical: 8.0),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                height: 20,
                                                width: 200,
                                                child: ListView(
                                                    scrollDirection: Axis.horizontal,
                                                    children:<Widget>[Text(ins,style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                                                    ),]
                                                ),
                                              ),
                                              Text(name,style: TextStyle(fontWeight: FontWeight.w200, fontSize:18),
                                              )
                                            ],
                                          ),
                                        ),
                                        VerticalDivider(
                                          indent: 5,
                                          endIndent: 5,
                                          thickness: 2,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 20),
                                          child: w,
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                          BlocBuilder(
                            bloc: navegarBloc,
                            builder: (context, NavegarState estado) {

                              if(estado is AgregandoSticker)
                                {
                                  List<Widget> widgets = [];
                                  return Stack(
                                    children: <Widget>[
                                    Align(alignment: Alignment.bottomCenter,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                       children: <Widget>[

                                           FlatButton(
                                             padding: EdgeInsets.zero,
                                             onPressed: () {
                                               navegarBloc.dispatch(ConfirmarCancelarUbicacion(false, null,null));
                                               },
                                             child: Padding(
                                               padding: const EdgeInsets.all(10.0),
                                               child: Container(
                                                   decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white,
                                                     boxShadow: [const BoxShadow(color: Colors.grey, blurRadius: 5.0)],),
                                                   child: Icon(Icons.close, color: Color(0xFFD53D3D),size: 38,)
                                               ),
                                             ),
                                           ),

                                         FlatButton(
                                           padding: EdgeInsets.zero,
                                           onPressed: () async {


                                             LatLng posSel = mapa.posSel;

                                             navegarBloc.dispatch(ConfirmarCancelarUbicacion(true, posSel,mapa));




                                              Scaffold.of(context).showSnackBar(SnackBar(content: Text("Se agregó el sticker")));
                                             },
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(vertical: 5.0),
                                            child: Container(
                                              decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white,
                                              boxShadow: [const BoxShadow(color: Colors.grey, blurRadius: 5.0)],),
                                              child: Icon(Icons.check, color: Color(0xFF32613E),size: 38,)
                                      ),
                                          ),
                                        ),
                                    ],

                                  )
                                  ),
                                  ],
                                  );
                                }
                              return Container();
                            }
                          )
                        ],
                      ),
                    ),

                    Container(
                      width: MediaQuery.of(context).size.width ,
                      height: MediaQuery.of(context).size.height * 0.08,
                      child:
                      BlocListener(
                        condition: (previous,other )
                          {
                            if(previous is InsCargada)
                              {
                                return true;
                              }
                            return false;
                          },
                        bloc: navegarBloc,
                        listener: (context, NavegarState estado)
                        {
                          if(estado is AgregandoSticker)
                            {
                              Scaffold.of(context).showSnackBar(SnackBar(content: Text("Presiona donde quieras agregar el sticker")));
                            }
                        },
                        child: BlocBuilder(
                            bloc: navegarBloc,
                            builder: (context, NavegarState state) {

                              if(state is InsCargada)
                                {
                                  return ListView(
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 5.0),
                                            child: Text("Tiempo",style: TextStyle(fontSize: 16)),
                                          ),
                                          Icon(Icons.timer),

                                          Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 5.0),
                                            child: Text("Distancia",style: TextStyle(fontSize: 16)),
                                          ),
                                          Image.asset("images/regla.png")

                                        ],

                                      )

                                    ],
                                  );
                                }

                              if(state is AgregandoSticker)
                                {
                                  //Scaffold.of(context).showSnackBar(SnackBar(content: Text("Presiona la ubicacion")));
                                  BoxDecoration deco = BoxDecoration(border: Border.all(color: Colors.grey), borderRadius: BorderRadius.all(Radius.circular(10)));
                                  return ListView.builder(scrollDirection: Axis.horizontal,itemCount: 3,itemBuilder: (BuildContext context, int index) {


                                    Icon icono;
                                    switch(index)
                                    {
                                      case 0:
                                        icono = Icon(Icons.local_parking,size: 20,);
                                        break;

                                      case 1:
                                        icono = Icon(Icons.warning,size: 20,);
                                        break;
                                      case 2:
                                        icono = Icon(Icons.directions_walk,size: 20,);
                                    }


                                      return Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: OpcionSticker(stickers[index], icono),
                                      );
                                  }
                                  );
                                }
                              return Container();
                            }
                          )

                      ) ,
                    ),


                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, top: 10, right: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          ButtonTheme(
                            height: 70,
                            minWidth: MediaQuery.of(context).size.width * 0.45,
                            child: FlatButton.icon(
                              icon: Icon(
                                Icons.warning,
                                color: Colors.white,
                                size: 40,
                              ),
                              color: Color.fromRGBO(252, 102, 102, 1),
                              onPressed: () => {},
                              label: Padding(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width / 20),
                                child: InkWell(
                                  onTap: () => launch("tel://123"),
                                  child: Text('EMERGENCIA',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          letterSpacing: 2,
                                          fontWeight: FontWeight.w300)),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: MediaQuery.of(context).size.width * 0.03),
                          Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width * 0.15,
                            decoration: BoxDecoration(
                                border: Border.all(color: w.amarillo),
                                borderRadius: BorderRadius.all(Radius.circular(25))),
                            child: IconButton(
                              onPressed: () async {


                                navegarBloc.dispatch(AgregarSticker());
                                /*
                                  LatLng posSel = await mapa.getPosicion(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height * 0.7).catchError((e){print(e);});
                                  print(posSel);
                                  */

                              },
                              icon: Icon(
                                Icons.group,
                                size: 35,
                                color: Colors.grey,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )),
        );
      }
    );
  }
  
}


class OpcionSticker extends StatefulWidget {

  final String texto;
  final Widget icono;

  OpcionSticker(this.texto, this.icono);

  @override
  _OpcionStickerState createState() => _OpcionStickerState();
}

class _OpcionStickerState extends State<OpcionSticker> {

  String sel;

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: BlocProvider.of<NavegarBloc>(context),
      listener: (context, NavegarState estado)
      {

        if(estado is AgregandoSticker)
          {

            sel = estado.sel;
            print(sel);
          }
      },
      child: GestureDetector(
        onTap: (){
          BlocProvider.of<NavegarBloc>(context).dispatch(SeleccionarSticker(widget.texto));
        },
        child: Container(
          width: MediaQuery.of(context).size.width * 0.32,
          decoration: BoxDecoration(border: Border.all(color: widget.texto== sel||(sel ==null&&widget.texto == "HUECO") ? amarillo :Colors.grey), borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                widget.icono,
                Text(widget.texto),
              ],
            ),
          ) ,
        ),
      ),
    );
  }
}


