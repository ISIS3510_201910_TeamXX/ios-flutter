import 'package:ciclap/Interfaz/Bluetooth.dart';
import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/VOs/LugarVO.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ciclap/Interfaz/widgets.dart';
import 'package:ciclap/Interfaz/VisualizarTerreno.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/services.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:flutter_typeahead/cupertino_flutter_typeahead.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

const Color negro = Color(0xFF353531);
const Color amarillo = Color(0xFFFBBC05);
const Color morado = Color(0xFF5F0F40);

class PonerDestino extends StatefulWidget {
  @override
  _PonerDestinoState createState() => _PonerDestinoState();
}

class _PonerDestinoState extends State<PonerDestino> {
  final destinoBloc = DestinoBloc();
  TextEditingController editControl1 = TextEditingController();
  TextEditingController editControl2 = TextEditingController();
  TextEditingController editControlP = TextEditingController();

  bool cicloruta =false;

  BuildContext contextoScaffold;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DestinoBloc>(
      builder: (context) => destinoBloc,
      child: Scaffold(
          resizeToAvoidBottomPadding: false,
          body: new Builder(builder: (BuildContext context) {
            contextoScaffold = context;
            return BlocListener(
              bloc: destinoBloc,
              listener: (BuildContext context, DestinoState estado) {
                if (estado is ListoParaEnviar) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => VisualizarTerreno(
                              ruta: estado.ruta, conParada: estado.conParada)))
                    ..whenComplete(() {
                      destinoBloc.dispatch(RebuildSeleccionada(
                          estado.anteriorEstado.anteriorEstado));
                    });
                }
              },
              child: BlocBuilder(
                bloc: destinoBloc,
                builder: (BuildContext context, DestinoState estado) {
                  if (estado is CargandoRuta) {
                    return Stack(
                      children: <Widget>[
                        pantalla(),
                        Opacity(
                          opacity: 0.5,
                          child: ModalBarrier(
                            color: Colors.grey,
                            dismissible: false,
                          ),
                        ),
                        Center(
                          child: CupertinoActivityIndicator(
                            radius: 20,
                          ),
                        )
                      ],
                    );
                  } else if (estado is Error) {
                    return MensajeError(pantalla(), estado.error, aceptarError);
                  } else {
                    return pantalla();
                  }
                },
              ),
            );
          })),
    );
  }

  void aceptarError() {
    print("Entra a aceptar error");

    Error estado = destinoBloc.currentState;
    CargandoRuta anterior = estado.anteriorEstado;

    destinoBloc.dispatch(RebuildSeleccionada(anterior.anteriorEstado));
  }

  Widget pantalla() {
    return SafeArea(
      child: OfflineBuilder(
        child: Container(),
        connectivityBuilder:(
            BuildContext context,
            ConnectivityResult connectivity,
            Widget child,
            ){
          final bool connected = connectivity != ConnectivityResult.none;

          return Column(
            children: <Widget>[
              Container(
                height: 40,
                child: Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    Positioned(left: 10, child: BackButton(color: Colors.black,)),
                    Visibility(
                      visible: !connected,
                      child: Align(alignment: Alignment.center,
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("Sin conexión a internet", style: TextStyle(),),
                          ),
                          decoration: BoxDecoration(color: Color(0xFFFFABAB), boxShadow: [new BoxShadow(color: Colors.grey, blurRadius: 5.0)] ),
                        ) ,),
                    )
                  ],
                ),
              ),


              SearchBar(
                textoHint: "Ingresa el punto de partida",
                parada: false,
                controller: editControl1,
                puntoInicio: true, disabled: !connected,
              ),
              BlocListener(
                bloc: destinoBloc,
                listener: (context, estado) {
                  if (estado is SeleccionadaUbicacion) {
                    editControl1.text = estado.miUbicacion.nombre;
                  }
                },
                child: BlocBuilder(
                    bloc: destinoBloc,
                    builder: (BuildContext context, DestinoState estado) {
                      return Visibility(
                        child: SearchBar(
                          textoHint: "Agrega el punto de parada",
                          parada: true,
                          controller: editControlP,
                          puntoInicio: false, disabled: false,
                        ),
                        visible: estado.conParada ?? false,
                      );
                    }),
              ),
              SearchBar(
                textoHint: "Ingresa el destino",
                parada: false,
                controller: editControl2,
                puntoInicio: false,disabled: false,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  RawMaterialButton(
                    onPressed: () => destinoBloc.dispatch(AgregarParada()),
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: BlocBuilder(
                            bloc: destinoBloc,
                            builder: (BuildContext context, DestinoState estado) {
                              if (estado.conParada ?? false) {
                                return TextoPlus(
                                  texto: "QUITAR PARADA",
                                  quitar: true,
                                );
                              }
                              return TextoPlus(
                                texto: "AGREGAR PARADA",
                                quitar: false,
                              );
                            })),
                  ),
                  BotonPrimario(
                    texto: "OK",
                    color: amarillo,
                    onPressed: connected? navegar:null,
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Prefiero ir en ciclorruta',
                            style: TextStyle(fontSize: 20),
                          ),
                          Switch(
                            activeColor: Colors.white,
                            activeTrackColor: amarillo,
                            value: cicloruta,
                            onChanged: (value) {
                              setState(() {
                                cicloruta = !cicloruta;
                                print(cicloruta);
                              });
                            },
                          )
                        ],
                      ),
                      Text(
                        'Buscaremos una ruta para que vayas la mayoría del tiempo en ciclorruta',
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300),
                      ),
                    ]),
              ),



               /* BotonSecundario(
                    hijo: Container(
                     width: 170,
                      child: Row(children: <Widget>[Icon(Icons.bluetooth, color: Colors.blueAccent,), Text("Viajar Con un Amigo")])),
                      onPressed: () {Navigator.push(context,MaterialPageRoute(builder: (context) => Bluetooth() ));}),
*/


              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Container(width: MediaQuery.of(context).size.width,height: 3,color: Color(0xFFE9E9E9),),
              ),
              SizedBox(height: 5,),

              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    child: Text(
                      "Recientes (Mockups)",
                      style: TextStyle(fontSize: 18),
                    ),
                  )
                ],
              ),
              BlocListener(
                bloc: destinoBloc,
                listener: (context, DestinoState estado) {},
                child: BlocBuilder(
                    bloc: destinoBloc,
                    builder: (BuildContext context, DestinoState estado) {
                      if (estado is InitialDestinoState) {
                        destinoBloc.dispatch(Cargar(estado.conParada));
                        return Expanded(
                          child: Stack(
                            children: <Widget>[
                              Align(
                                child: CupertinoActivityIndicator(
                                  radius: 15,
                                ),
                                alignment: Alignment.center,
                              )
                            ],
                          ),
                        );
                      } else if (estado is RecientesCargados ||
                          estado is SeleccionadaUbicacion) {
                        RecientesCargados recienteE;
                        SeleccionadaUbicacion selE;

                        if (estado is RecientesCargados) {
                          recienteE = estado;
                        } else {
                          selE = estado;
                        }
                        return Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: grisSombra, width: 1),
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                              color: Colors.white,
                              boxShadow: [
                                new BoxShadow(color: grisClaro, blurRadius: 5.0)
                              ],
                            ),
                            child: buildRecientes(estado is RecientesCargados
                                ? recienteE.lugares
                                : selE.lugares));
                      }
                      return Container();
                    }),
              )
            ],
          );},
      ),
    );
  }

  Stack buildRecientes(List<Lugar> recientes) {
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.3,
          child: ListView.builder(
              itemCount: recientes.length,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        editControl2.text = recientes[index].nombre;
                        setState(() {});
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextoIcono(
                          textoOtroStyle: Text(
                            recientes[index].nombre,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.grey,
                                fontWeight: FontWeight.normal),
                          ),
                          icono: Icon(
                            Icons.access_time,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                    Divider(
                      height: 2,
                      endIndent: 12,
                      indent: 12,
                    )
                  ],
                );
              }),
        )
      ],
    );
  }

  void navegar() {
    String origenNombre = editControl1.text;
    String destinoNombre = editControl2.text;
    String paradaNombre = editControlP.text;
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    bool condicion = origenNombre == null ||
        origenNombre.isEmpty ||
        destinoNombre == null ||
        destinoNombre.isEmpty;

    DestinoState currentState = destinoBloc.currentState;
    final snackbar = SnackBar(
      content: Text("Coloque todos los lugares"),
    );

    Lugar lugarOrigen;
    Lugar lugarDestino;
    if (destinoBloc.currentState.conParada) {
      lugarOrigen;
      Lugar lugarParada;
      lugarDestino;
      if (!condicion && !(paradaNombre.isEmpty || paradaNombre == null)) {
        if (currentState is SeleccionadaUbicacion) {
          if (currentState.miUbicacion.ubicacion != null) {
            lugarOrigen = currentState.miUbicacion;
          } else {
            lugarOrigen = Lugar(nombre: origenNombre);
          }

          if (currentState.lugarDestino != null) {
            lugarDestino = currentState.lugarDestino;
          } else {
            lugarDestino = Lugar(nombre: destinoNombre);
          }
        } else {
          lugarOrigen = Lugar(nombre: origenNombre);
          lugarDestino = Lugar(nombre: origenNombre);
        }

        lugarParada = Lugar(nombre: paradaNombre);

        destinoBloc.dispatch(EnviarDestinoConParada(lugarOrigen: lugarOrigen,lugarDestino: lugarDestino,lugarParada: lugarParada, cicloruta: cicloruta));

      } else {
        Scaffold.of(contextoScaffold).showSnackBar(snackbar);
      }
    } else {
      //SIN PARADA
      if (!condicion) {
        if (currentState is SeleccionadaUbicacion) {
          if (currentState.miUbicacion != null &&
              currentState.miUbicacion.ubicacion != null) {
            lugarOrigen = currentState.miUbicacion;
          } else {
            lugarOrigen = Lugar(nombre: origenNombre);
          }

          if (currentState.lugarDestino != null &&
              currentState.lugarDestino.nombre != null) {
            lugarDestino = currentState.lugarDestino;
          } else {
            lugarDestino = Lugar(nombre: destinoNombre);
          }
        } else {
          //current state es RecientesCargados
          lugarOrigen = Lugar(nombre: origenNombre);
          lugarDestino = Lugar(nombre: destinoNombre);
        }

        destinoBloc.dispatch(EnviarDestino(lugarOrigen, lugarDestino, cicloruta));
      } else {
        Scaffold.of(contextoScaffold).showSnackBar(snackbar);
      }
    }
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    destinoBloc.dispose();
    editControlP.dispose();
    editControl1.dispose();
    editControl2.dispose();
    super.dispose();
  }
}
