import 'package:flutter/material.dart';
import 'package:ciclap/Interfaz/widgets.dart';

const Color negro = Color(0xFF353531);
const Color amarillo = Color(0xFFFBBC05);
const Color morado = Color(0xFF5F0F40);

class MisRutas extends StatefulWidget {
  @override
  _MisRutasState createState() => _MisRutasState();
}

class _MisRutasState extends State<MisRutas> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height * 0.4,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
                  //TODO: Child del mapa
                ),
                Positioned(     //Boton Back
                    top: 30,
                    left: 0,
                    child: FlatButton(
                        padding: EdgeInsets.zero,
                        onPressed: () {Navigator.pop(context);},
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                              boxShadow: [new BoxShadow(color: Colors.grey, blurRadius: 5.0)]),
                          child: Icon(Icons.arrow_back,size: 30,),
                        ))),
                Positioned(
                  right: 10,
                  top: MediaQuery.of(context).size.height * 0.3,
                  child: BotonPrimario(texto: "Ir", color: amarillo, onPressed: () => null,)

                )

              ],
            ),
          ]),


    );
  }


}