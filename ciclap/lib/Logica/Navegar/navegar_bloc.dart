import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:ciclap/VOs/InstruccionVO.dart';
import 'package:ciclap/Repositorio/RutaRepo.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:vibration/vibration.dart';
import 'dart:math' as math;
import '../bloc.dart';

class NavegarBloc extends Bloc<NavegarEvent, NavegarState> {

  Ruta ruta;
  RutaRepo repo = RutaRepo();
  int indice;
  List<Instruccion> instrucciones;

  @override
  NavegarState get initialState => InitialNavegarState();

  @override
  Stream<NavegarState> mapEventToState(
    NavegarEvent event,
  ) async* {
    String name;

    if(event is ComenzarNavegacion)
      {
        print("entra");
        String ins;
        Widget icono;
        try {

          ruta = event.ruta;
          ruta.latitudInicio = ruta.puntoSalida.latitud;
          ruta.longitudInicio = ruta.puntoSalida.longitud;

          if(ruta.conParada)
            {
              ruta.latitudParada = ruta.puntoParada.latitud;
              ruta.longitudParada = ruta.puntoParada.longitud;
            }

          ruta.latitudFinal = ruta.puntoFinal.latitud;
          ruta.longitudFinal = ruta.puntoFinal.longitud;

          print("UID EN BLOC ${event.uid}");
          repo.postRuta(ruta, event.uid);

          instrucciones = ruta.instrucciones;
          indice = 0;
          ins = instrucciones[indice].instruction;
          name = instrucciones[indice].name;

          if(name == "-")
            {
              name = "";
            }else{
            ins = ins.split(name)[0];

          }

          if (ins.contains("Right")) {

            icono = Transform.rotate(
                angle: 180 * math.pi / 180,
                child: Icon(
                  Icons.subdirectory_arrow_left,
                ));
          } else if(ins.contains("Left")){

            icono = Transform.rotate(
                angle: 180 * math.pi / 180,
                child: Icon(
                  Icons.subdirectory_arrow_right,
                ));
          }else{
            icono =  Icon(Icons.arrow_upward,);
          }
        }catch(error)
        {
          print("ERROR EN BLOC" + error);
        }


        print("INSTRUCCION" + ins);
        yield InsCargada(ins, name,icono);
      }


    if(event is SiguienteInstruccion)
      {

        String ins;
        Widget icono;
        indice++;
        if(indice==instrucciones.length)
          {
            //TERMINO Reccorido
          }else{

          ins = instrucciones[indice].instruction;
          name = instrucciones[indice].name;

          if(name == "-")
          {
            name = "";
          }else{
            ins = ins.split(name)[0];

          }

          if (ins.contains("right")) {


            try{

              if (await Vibration.hasVibrator()) {
              Vibration.vibrate();
              Future.delayed(Duration(seconds: 1), (){Vibration.vibrate();});

            }}catch(error){print(error);}


            icono = Transform.rotate(
                angle: 180 * math.pi / 180,
                child: Icon(
                  Icons.subdirectory_arrow_left,
                ));
          } else if(ins.contains("left")){

            if (await Vibration.hasVibrator()) {
              Vibration.vibrate();

            }
            icono = Transform.rotate(
                angle: 180 * math.pi / 180,
                child: Icon(
                  Icons.subdirectory_arrow_right,
                ));
          }else{
            icono =  Icon(Icons.arrow_upward,);
          }

          yield InsCargada(ins, name,icono);
        }


        //yield InsCargada(ins);

      }

    if(event is AgregarSticker)
      {
        InsCargada estado = currentState;
        yield(AgregandoSticker(estado.instruccion, estado.name, estado.widget, "HUECO"));
      }

    if(event is SeleccionarSticker)
      {
        print("${event.stickerSel}");

        var actual = (currentState as AgregandoSticker);

        var nuevo = AgregandoSticker(actual.instruccion,actual.name, actual.widget, event.stickerSel);
        yield(nuevo);
      }

    if(event is ConfirmarCancelarUbicacion)
      {
          if(event.confirmar)
            {

              var actual = (currentState as AgregandoSticker);
              print(event.ubicacion);

              RutaRepo().postMarcadores(actual.sel?? "HUECO", GeoPoint(event.ubicacion.latitude, event.ubicacion.longitude));
              BitmapDescriptor iconoParqueo = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),"images/parking.png");
              BitmapDescriptor iconoBajarse = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),"images/caminar.png");
              BitmapDescriptor iconoHueco = await BitmapDescriptor.fromAssetImage(ImageConfiguration(),"images/hueco.png");

              var icono;
              switch(actual.sel){
                case "HUECO":
                  icono = iconoHueco;
                  break;

                case "BAJARSE":
                  icono = iconoBajarse;
                  break;

                case "PARQUEO":
                  icono =iconoParqueo;
                  break;
              }
              event.mapa.markers.add(Marker(markerId: MarkerId(event.ubicacion.toString()), position: event.ubicacion , icon: icono));
              yield(InsCargada(actual.instruccion, actual.name, actual.widget));
            }else{
            AgregandoSticker estado = currentState;
            yield(InsCargada(estado.instruccion, estado.name, estado.widget));
          }


      }



  }
}
