import 'package:ciclap/VOs/PuntoInteresVO.dart';
import 'package:ciclap/VOs/RutaVO.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PiState extends Equatable{

  PiState([List props = const[]]):super(props);
}

class InitialPiState extends PiState {}

class CargandoPI extends PiState{

}

class CargadosPI extends PiState{

  final List<PuntoInteres> talleres;
  List<PuntoInteres> cafeterias;
  List<PuntoInteres> restaurantes;
  List<PuntoInteres> puntosDeInteres;
  final Seleccionado sel;
  final bool talleresCargados;
  final bool cafeteriasCargadas;
  final bool restaurantesCargados;



  CargadosPI(this.talleres, this.sel, this.talleresCargados, this.cafeteriasCargadas, this.restaurantesCargados,[this.cafeterias, this.restaurantes]):super([talleres,sel,restaurantesCargados,cafeteriasCargadas,talleresCargados]);
}



class CargandoNavegar extends PiState{


}

class NavegarPI extends PiState{

  final Ruta ruta;
  CargadosPI anteriorEstado;


  NavegarPI(this.ruta, this.anteriorEstado):super([ruta, anteriorEstado]);

}

class PiError extends PiState
{
  final String mensaje;

  PiError(this.mensaje):super([mensaje]);


}

class Seleccionado
{
  int sel;
  String tipo;

  Seleccionado(this.sel,this.tipo);

}
