import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:ciclap/Interfaz/Mapa.dart';
import 'package:ciclap/Repositorio/AutenticacionRepo.dart';
import 'package:ciclap/VOs/UsuarioVO.dart';
import '../bloc.dart';
import 'package:ciclap/Repositorio/UsuarioRepo.dart';
import 'package:meta/meta.dart';

class AutenticacionBloc extends Bloc<AutenticacionEvent, AutenticacionState> {

  @override
  AutenticacionState get initialState => Uninitialized();

  final AutenticacionRepo _userRepository;
  GoogleMapa mapa = GoogleMapa(false);

  AutenticacionBloc({@required AutenticacionRepo userRepository})
      : _userRepository = userRepository;


  @override
  Stream<AutenticacionState> mapEventToState(
      AutenticacionEvent event,
      ) async* {
    if (event is AppStarted) {
      yield* _mapAppStartedToState();
    } else if (event is LoggedIn) {
      yield* _mapLoggedInToState();
    } else if (event is LoggedOut) {
      yield* _mapLoggedOutToState();
    }
  }

  Stream<AutenticacionState> _mapAppStartedToState() async* {
    try {
      final isSignedIn = await _userRepository.isSignedIn();
      if (isSignedIn) {
        final uid = await _userRepository.getUid();
        Usuario user = await UsuarioRepo("usuarios").getUsuario(uid);
        yield Authenticated(user, uid);
      } else {
        yield Unauthenticated();
      }
    } catch (_) {
      yield Unauthenticated();
    }
  }

  Stream<AutenticacionState> _mapLoggedInToState() async* {
    var uid = await _userRepository.getUid();
    Usuario user = await UsuarioRepo("usuarios").getUsuario(uid);
    yield Authenticated(user, uid);
  }

  Stream<AutenticacionState> _mapLoggedOutToState() async* {
    yield Unauthenticated();
    _userRepository.signOut();
  }
}

