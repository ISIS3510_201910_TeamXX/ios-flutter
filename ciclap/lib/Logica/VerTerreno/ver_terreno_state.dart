import 'package:ciclap/VOs/RutaVO.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class VerTerrenoState extends Equatable {

  VerTerrenoState([List props = const[]]):super(props);
}

class InitialVerTerrenoState extends VerTerrenoState {


}

class RutasCargando extends VerTerrenoState{
  final Ruta ruta;

  RutasCargando(this.ruta):assert(ruta!=null),super([ruta]);

}

class RutaPintada extends VerTerrenoState{
  final Ruta ruta;

  RutaPintada(this.ruta):super([ruta]);

}




class Navegando extends VerTerrenoState{

  final Ruta ruta;
  final Widget mapa;

  Navegando(this.ruta, this.mapa):super([ruta, mapa]);

}

class setEstado extends VerTerrenoState{



}