import 'package:ciclap/Interfaz/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';




class Comunidad extends StatefulWidget {
  @override
  _ComunidadState createState() => _ComunidadState();
}

class _ComunidadState extends State<Comunidad> {
  @override
  Widget build(BuildContext context) {
    return
          Scaffold(
           body: Padding(
             padding: const EdgeInsets.symmetric(vertical: 20),
            child: Column(

            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height*0.1,
                decoration: BoxDecoration(border: Border.all(color: Color(0xfff0f0f0), width: 1), borderRadius: BorderRadius.all(Radius.circular(5)),color: Colors.white, boxShadow: [
                  new BoxShadow(
                      color: Color(0xfff0f0f0),
                      blurRadius: 5.0
                  )
                ],
                ),
                child: SafeArea(
                    child:Stack(
                    children: <Widget>[
                     Align(
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("La comunidad ", style: TextStyle(fontSize: 24),),
                          Column(
                            children: <Widget>[
                              Image.asset("images/logoAmarillo.png", height: 35,),
                              SizedBox(height: 1,)
                            ],
                          )
                        ],
                    ),

                    ),
                      Positioned(
                      left: 5,
                      child: BackButton(),
                    )
                  ],
                ))
              ),
              
              Padding(
                padding: const EdgeInsets.all(25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Cómo contribiur\ncon la comunidad", style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),),
                  ],
                ),
              ),

              ListTile(
                title: Text("Informa", style: TextStyle(fontSize: 22)),
                subtitle:Text("Cuando estés en un viaje presiona este botón para reportar que en ese lugar hay un hueco,un accidente, personas sospechosas entre otras.") ,

            ),
              Divider(color: Colors.grey,height: 30,),
              ListTile(
                title: Text("Califica",style: TextStyle(fontSize: 22)),
                subtitle:Text("Al final de cada viaje no olvides calificar el nivel de dificultad del tramo que recorriste y la comodidad que tuviste") ,
              ),
              Divider(color: Colors.grey,height: 30,),
              ListTile(
                title: Text("Agrega",style: TextStyle(fontSize: 22)),
                subtitle:Text("Agrega lugares de interés para otros ciclistas ") ,
                trailing: BotonPrimario(color: morado,texto: "Agregar",onPressed: ()=> null,),
              ),
              Divider(color: Colors.grey,height: 30,),
              ListTile(
                title: Text("Explora",style: TextStyle(fontSize: 22)),
                subtitle:Text("Mira los lugares mejores calificados por los usuarios") ,
                trailing: BotonPrimario(color: morado, texto: "Explorar",onPressed: () => null,),

              )




          ],

        ),
      ),
    );
  }
}
