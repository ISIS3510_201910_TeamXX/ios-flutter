import 'dart:math';

import 'package:ciclap/Interfaz/AgregarPuntoInteres.dart';
import 'package:ciclap/Interfaz/Mapa.dart';
import 'package:ciclap/Interfaz/VisualizarTerreno.dart';
import 'package:ciclap/Logica/bloc.dart';
import 'package:ciclap/Repositorio/PIrepo.dart';
import 'package:ciclap/VOs/PuntoInteresVO.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ciclap/Interfaz/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

const Color negro = Color(0xFF353531);
const Color amarillo = Color(0xFFFBBC05);
const Color morado = Color(0xFF5F0F40);

class PuntosInteres extends StatefulWidget {
  @override
  _PuntosInteresState createState() => _PuntosInteresState();
}

class _PuntosInteresState extends State<PuntosInteres>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  PiBloc piBloc = PiBloc();
  GoogleMapa mapa;
  @override
  void initState() {
    super.initState();
    tabController = TabController(vsync: this, length: 3);
    mapa =BlocProvider.of<AutenticacionBloc>(context).mapa;
    mapa.dibujarRuta = false;
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AgregarPI()));
          },
          child: Icon(Icons.add),
          backgroundColor: Colors.blueAccent,),

      body: BlocProvider(
        builder: (context) => piBloc..dispatch(CargarPi()),
        child: BlocListener(
          bloc: piBloc,
          listener: (BuildContext contexto, PiState estado){

            if(estado is NavegarPI)
              {

                Navigator.push(context,MaterialPageRoute(builder: (context) => VisualizarTerreno(ruta: estado.ruta,conParada: false,))).whenComplete((){piBloc.dispatch(RebuildPi());});
              }

          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.4,
                    width: MediaQuery.of(context).size.width,
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.grey)),
                    child: GoogleMapa(false),
                  ),
                  Positioned(
                      top: 30,
                      left: 0,
                      child: FlatButton(
                          padding: EdgeInsets.zero,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  new BoxShadow(color: Colors.grey, blurRadius: 5.0)
                                ]),
                            child: Icon(
                              Icons.arrow_back,
                              size: 30,
                            ),
                          )))
                ],
              ),

                  BlocBuilder(
                    bloc:piBloc,
                    builder: (BuildContext context, PiState estado) {

                      if(estado is CargandoPI)
                        {
                          return Center(child: CupertinoActivityIndicator(radius: 16,),);
                        }else{
                        return DefaultTabController(length: 3,
                            child:Column(
                              children: <Widget>[
                                TabBar(indicatorColor: amarillo, tabs: [
                                  Tab(text: "Talleres",),
                                  Tab(text: "Cafeterías"),
                                  Tab(text: "Restaurantes",)
                                ]),
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: MediaQuery.of(context).size.height *0.50,
                                    child: TabBarView(
                                        children:   [Talleres(), Cafeterias(), Restaurantes()]))
                              ],

                            )
                        );
                      }

                    }
                  )
            ],
          ),
        ),
      ),
    );
  }
}

class Talleres extends StatefulWidget {
  @override
  _TalleresState createState() => _TalleresState();
}

class _TalleresState extends State<Talleres> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: BlocProvider.of<PiBloc>(context),
      builder:(BuildContext context, PiState estado) {

        if(estado is PiError)
          {
            return Center(child: Text(estado.mensaje));
          }

        if(!(estado is CargadosPI))
          {
            return Center(child: CupertinoActivityIndicator(radius: 15,),);
          }

        CargadosPI state = estado;

        return ListView.builder(
          itemCount:state.talleres.length ,
        itemBuilder: (BuildContext ctxt, int index)
        {
          return CardPuntoInteres(state.talleres[index],  index);
       }
      );},
    );
  }
}

class Cafeterias extends StatefulWidget {
  @override
  _CafeteriasState createState() => _CafeteriasState();
}

class _CafeteriasState extends State<Cafeterias> {

  @override
  void initState() {

    var estado = BlocProvider.of<PiBloc>(context).currentState;
    if(estado is CargadosPI)
    {
      if(!estado.restaurantesCargados)
      {
        BlocProvider.of<PiBloc>(context).dispatch(CargarCafe());
      }
    }

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        child:  BlocBuilder(
            bloc:BlocProvider.of<PiBloc>(context),
            builder: (context, PiState state) {
              if(state is CargadosPI)
              {

                if(state.cafeteriasCargadas)
                {

                  return ListView.builder(
                      itemCount:state.cafeterias.length ,
                      itemBuilder: (BuildContext ctxt, int index)
                      {
                        return CardPuntoInteres(state.cafeterias[index],  index);
                      }
                  );
                }

              }else{
                return CupertinoActivityIndicator(radius: 15,);
              }

                return Container();
            }
        ),

    );
  }
}

class Restaurantes extends StatefulWidget {
  @override
  _RestaurantesState createState() => _RestaurantesState();
}

class _RestaurantesState extends State<Restaurantes> {

  @override
  void initState() {

    var estado = BlocProvider.of<PiBloc>(context).currentState;
    if(estado is CargadosPI)
      {
        if(!estado.restaurantesCargados)
          {
            BlocProvider.of<PiBloc>(context).dispatch(CargarRestaurante());
          }
      }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child:  BlocBuilder(
          bloc:BlocProvider.of<PiBloc>(context),
          builder: (context, PiState state) {
            if(state is CargadosPI)
            {

              if(state.restaurantesCargados)
              {

                return ListView.builder(
                    itemCount:state.restaurantes.length ,
                    itemBuilder: (BuildContext ctxt, int index)
                    {
                      return CardPuntoInteres(state.restaurantes[index],  index);
                    }
                );
              }

            }else{
              return CupertinoActivityIndicator(radius: 15,);
            }

            return Container();
          }
      ),

    );

  }
}


class CardPuntoInteres extends StatefulWidget {
  final PuntoInteres pi;
  final int indice;


  CardPuntoInteres(this.pi, this.indice);
  @override
  _CardPuntoInteresState createState() => _CardPuntoInteresState();
}

class _CardPuntoInteresState extends State<CardPuntoInteres> {

  int indiceSel;
  int resenias;
  int estrellas;



  @override
  Widget buildEstrellas(BuildContext ctxt, int index) {
    return new Icon(Icons.star_border, color: amarillo, size: 15);
  }


  @override
  void initState() {
    // TODO: implement initState
    resenias = widget.pi.numResenias;
    estrellas =  widget.pi.estrellas;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: BlocProvider.of<PiBloc>(context),
      listener: (BuildContext context, PiState estado)
      {
        if(estado is CargadosPI)
          {
            if(estado.sel != null)
              {
                indiceSel = estado.sel.sel;
              }
          }
      },
      child: GestureDetector(
        onTap: (){print("espicha ${widget.indice} ${indiceSel}");BlocProvider.of<PiBloc>(context).dispatch(SeleccionarPi(Seleccionado(widget.indice,widget.pi.categoria)));},
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            //height: MediaQuery.of(context).size.height * 0.13,
            decoration: BoxDecoration(border: Border.all(color: indiceSel == widget.indice&&indiceSel!=null ? amarillo: Colors.grey),
                color: indiceSel == widget.indice&&indiceSel!=null? Colors.white:Color(0xFFEEEEE), boxShadow: indiceSel == widget.indice&&indiceSel!=null? [new BoxShadow(color: Colors.grey, blurRadius: 6.0)]: null ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircleAvatar(
                    backgroundImage: AssetImage("images/taller.png"), //RutaImagen
                    radius: MediaQuery.of(context).size.width * 0.08,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical:8.0),
                      child: Text(
                        widget.pi.nombre,
                        style: TextStyle(
                            color: negro, fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ),
                    //Nombre lugar
                    Text(widget.pi.direccion),
                    //Dirección
                    Row(
                      children: <Widget>[
                        Container(
                          width: 80,
                          height: 15,
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: estrellas,
                              itemBuilder: (BuildContext ctxt, int index) =>
                                  buildEstrellas(
                                      ctxt, index)),
                        ),
                        Text(
                          resenias.toString() +" reseña(s)",
                          style: TextStyle(color: Colors.grey, fontSize: 10),
                        )
                      ],
                    ),
                    Visibility(
                      visible: indiceSel == widget.indice&&indiceSel!=null ,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical:8.0),
                          child: Column(
                            children: <Widget>[

                              Container(width: MediaQuery.of(context).size.width * 0.4,child: Text(widget.pi.descripcion, style: TextStyle(fontSize: 12, color: Colors.grey),)),
                              Text("Tel: ${widget.pi.telefono == 0? "No disponible": widget.pi.telefono }",)

                            ],
                          ),
                        )

                    ),

                  ],
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.03,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    BotonPrimario(
                      texto: "Ir",
                      color: morado,
                      onPressed: (){BlocProvider.of<PiBloc>(context).dispatch(IrPi(widget.pi));},
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Visibility(
                        visible: indiceSel == widget.indice&&indiceSel!=null,
                        child: GestureDetector(
                            child: Text("Califica",style: TextStyle(color: Colors.amber,fontWeight: FontWeight.w500,fontSize: 15),),
                            onTap: (){
                              showDialog(
                                context:context,
                                builder:(BuildContext context)
                                  {
                                    return AlertDialog(title: Text("Califica este punto"),

                                        content: RatingBar(
                                          initialRating: 3,
                                          direction: Axis.horizontal,
                                          allowHalfRating: true,
                                          itemCount: 4,
                                          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                                          itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                              size:4

                                          ),
                                          onRatingUpdate: (rating) {

                                            print(rating);
                                            print(estrellas);
                                            estrellas = (((estrellas * resenias) + rating)/(resenias+1)).floor();
                                            print((estrellas * resenias + (rating))/(resenias+1));
                                            print(estrellas);
                                            resenias++;

                                            setState(() {

                                            });
                                             Navigator.pop(context);

                                          },
                                        ));;
                                  }

                              );



                            },

                        )

                      ),
                    ),

                    //Distancia
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

}


